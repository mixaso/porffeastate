<?php
/**
 * @package    oakcms
 * @author     Hryvinskyi Volodymyr <script@email.ua>
 * @copyright  Copyright (c) 2015 - 2017. Hryvinskyi Volodymyr
 * @version    0.0.1-beta.0.1
 */

namespace app\modules\casecontent;

use app\components\events\FetchRoutersEvent;
use app\modules\menu\behaviors\MenuUrlRule;
use app\components\module\ModuleEventsInterface;
use app\modules\admin\widgets\events\MenuItemsEvent;
use app\modules\admin\widgets\Menu;
use app\modules\casecontent\components\MenuRouterCaseContent;
use app\modules\menu\widgets\events\MenuItemRoutesEvent;
use app\modules\menu\widgets\MenuItemRoutes;
use Yii;

/**
 * content module definition class
 */
class Module extends \app\components\module\Module implements ModuleEventsInterface
{
    /** @var array The rules to be used in Backend Url management. */
    public static $urlRulesBackend = [];

    /** @var array The rules to be used in Frontend Url management. */
    public static $urlRulesFrontend = [
        'api/page/<slug:[\w\-]+>'                      => 'casecontent/api-page/view',
        'api/casecontent/<catslug:[\w\-]+>/<slug:[\w\-]+>' => 'casecontent/api-article/view',
        'api/casecontent/<slug:[\w\-]+>'                   => 'casecontent/api-category/view',

        'casecontent/tag/<tag:(.*)>'                       => 'casecontent/article/tag',
        'casecontent/search'                               => 'casecontent/search/search',
        'casecontent/<slug:[\w\-]+>'                       => 'casecontent/category/view',
        'casecontent/<catslug:[\w\-]+>/<slug:[\w\-]+>'     => 'casecontent/article/view',
        'casecontent/page/<slug:[\w\-]+>'                  => 'casecontent/page/view',
    ];

    public $settings = [
        'show_title'           => [
            'type'  => 'checkbox',
            'value' => false,
        ],
        'link_titles'          => [
            'type'  => 'checkbox',
            'value' => false,
        ],
        'show_intro'           => [
            'type'  => 'checkbox',
            'value' => false,
        ],
        'show_category'        => [
            'type'  => 'checkbox',
            'value' => false,
        ],
        'link_category'        => [
            'type'  => 'checkbox',
            'value' => false,
        ],
        'show_parent_category' => [
            'type'  => 'checkbox',
            'value' => false,
        ],
        'link_parent_category' => [
            'type'  => 'checkbox',
            'value' => false,
        ],
        'show_author'          => [
            'type'  => 'checkbox',
            'value' => false,
        ],
        'link_author'          => [
            'type'  => 'checkbox',
            'value' => false,
        ],
        'show_create_date'     => [
            'type'  => 'checkbox',
            'value' => false,
        ],
        'show_modify_date'     => [
            'type'  => 'checkbox',
            'value' => false,
        ],
        'show_publish_date'    => [
            'type'  => 'checkbox',
            'value' => false,
        ],
        'show_hits'            => [
            'type'  => 'checkbox',
            'value' => false,
        ],
        'categoryThumb'        => [
            'type'  => 'checkbox',
            'value' => false,
        ],
        'category_items_order' => [
            'type' => 'select',
            'value' => 0,
            'items' => [
                'rdate' => 'Latest First',
                'date' => 'Latest Last',
                'rmodified' => 'Modified First',
                'modified' => 'Modified Last',
                'alpha' => 'Alphabetical',
                'ralpha' => 'Alphabetical Reversed',
                'hits' => 'Most Hits',
                'rhits' => 'Least Hits',
                'random' => 'Random'
            ]
        ]
    ];

    /**
     * @param $event MenuItemsEvent
     */
    public function addAdminMenuItem($event)
    {
        $event->items['casecontent'] = [
            'label' => \Yii::t('casecontent', 'Casecontent'),
            'icon'  => '<i class="fa fa-file-text-o"></i>',
            'items' => [
                [
                    'icon'  => '<i class="fa fa-file-o"></i>',
                    'label' => \Yii::t('casecontent', 'CasePages'),
                    'url'   => ['/admin/casecontent/pages/index'],
                ],
                [
                    'icon'  => '<i class="fa fa-file-text-o"></i>',
                    'label' => \Yii::t('content', 'CaseArticles'),
                    'url'   => ['/admin/casecontent/article/index'],
                ],
                [
                    'icon'  => '<i class="fa fa-folder-o"></i>',
                    'label' => \Yii::t('content', 'CaseCategories'),
                    'url'   => ['/admin/casecontent/category/index'],
                ],
            ],
        ];
    }

    /**
     * @param $event
     */
    public function addFieldRelationModel($event)
    {
        $event->items['app\modules\casecontent\models\ContentArticles'] = Yii::t('casecontent', 'Case Content Articles');
        $event->items['app\modules\casecontent\models\ContentPages'] = Yii::t('casecontent', 'Case Content Pages');
        $event->items['app\modules\casecontent\models\ContentCategory'] = Yii::t('casecontent', 'Case Content Category');
    }

    /**
     * @param $event MenuItemRoutesEvent
     */
    public function addMenuItemRoutes($event)
    {
        $event->items['casecontent'] = [
            'label' => Yii::t('casecontent', 'Casecontent'),
            'items' => [
                [
                    'label' => Yii::t('casecontent', 'Case Category View'),
                    'url'   => [
                        '/admin/casecontent/category/select',
                    ],
                ],
                [
                    'label' => Yii::t('casecontent', 'Case Article View'),
                    'url'   => [
                        '/admin/casecontent/article/select',
                    ],
                ],
                [
                    'label' => Yii::t('casecontent', 'Case Page View'),
                    'url'   => [
                        '/admin/casecontent/pages/select',
                    ],
                ],
            ],
        ];
    }

    /**
     * @param $event FetchRoutersEvent
     */
    public function addMenuRouter($event)
    {
        $event->routers['MenuRouterCaseContent'] = MenuRouterCaseContent::className();
    }

    /**
     * @inheritdoc
     */
    public function events()
    {
        return [
            Menu::EVENT_FETCH_ITEMS                 => 'addAdminMenuItem',
            MenuItemRoutes::EVENT_FETCH_ITEMS       => 'addMenuItemRoutes',
            MenuUrlRule::EVENT_FETCH_MODULE_ROUTERS => 'addMenuRouter',
            \app\modules\field\Module::EVENT_RELATION_MODELS => 'addFieldRelationModel',
        ];
    }


    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
