$(function () {

    $('.caseItem__first_text_in , .caseItem__second_media , .caseItem__second_text').scroolly([{
        minWidth: 650,
        from: 'doc-top = vp-top',
        css: {
            left: 0,
            opacity: '1',
            position: 'relative'
        }
    }]);

    $('.caseItem__third_media_monitor').scroolly([{
        minWidth: 650,
        from: 'el-top = vp-center +50',
        css: {
            opacity: '1'
        }
    },
    {
        minWidth: 650,
        to: 'el-top = vp-center +40',
        css: {
            opacity: '0'
        }
    }]);

    $('.caseItem__third_media_women').scroolly([{
        minWidth: 650,
        from: 'el-top = vp-center +50',
        css: {
            opacity: '1',
            left: '440px',
            transition: 'opacity 2s, left 3s'
        }
    },
    {
        minWidth: 650,
        to: 'el-top = vp-center +40',
        css: {
            opacity: 0,
            left: '100%',
            transition: 'opacity 2s, left 3s'
        }
    }]);

    $('.caseItem__third_media_man').scroolly([{
        minWidth: 650,
        from: 'el-top = vp-center +50',
        css: {
            opacity: '1',
            'left': '640px',
            transition: 'opacity 2s, left 3s'
        }
    },
    {
        minWidth: 650,
        to: 'el-top = vp-center +40',
        css: {
            opacity: '0',
            'left': '100%',
            '-transition': 'opacity 2s, left 3s'
        }
    }]);

    $('.caseItem__third_text').scroolly([{
        maxWidth: 1199,
        minWidth: 650,
        from: 'el-center = vp-bottom',
        css: {
            opacity: '1'
        }
    },
    {
        minWidth: 1200,
        from: 'el-top = vp-center -200',
        css: {
            opacity: '1'
        }
    },
    {
        maxWidth: 1199,
        minWidth: 650,
        to: 'el-center = vp-bottom -10',
        css: {
            opacity: '0'
        }
    },
    {
        minWidth: 1200,
        to: 'el-top = vp-center -210',
        css: {
            opacity: '0'
        }
    }]);

    $('.caseItem__fourth_media_1').scroolly([{
        minWidth: 650,
        from: 'con-top = vp-bottom -120',
        css: {
            opacity: '1',
            '-transition': 'opacity 2s'
        }
    },
    {
        minWidth: 650,
        to: 'con-top = vp-bottom -130',
        css: {
            opacity: '0',
            '-transition': 'opacity 2s'
        }
    }], $('.caseItem__fourth_media_start'));

    $('.caseItem__fourth_media_2').scroolly([{
        minWidth: 650,
        from: 'con-top = vp-bottom -120',
        css: {
            opacity: '1',
            '-transition': 'opacity 2s'
        }
    },
    {
        minWidth: 650,
        to: 'con-top = vp-bottom',
        css: {
            opacity: '0',
            '-transition': 'opacity 2s'
        }
    },
    {
        minWidth: 900,
        from: 'con-center = vp-center +50con',
        css: {
            'left': '219px',
            'top': '-8px',
            '-transition': 'left .5s, top .5s',
            '-transition-duration': '1s'
        }
    },
    {
        minWidth: 900,
        to: 'con-center = vp-center +45con',
        css: {
            'left': '89px',
            'top': '48px',
            '-transition': 'left .5s, top .5s',
            '-transition-duration': '1s'
        }
    }], $('.caseItem__fourth_media_start'));

    $('.caseItem__fourth_media_3').scroolly([{
        minWidth: 650,
        from: 'con-top = vp-bottom -120',
        css: {
            opacity: '1',
            '-transition': 'opacity 2s'
        }
    },
    {
        minWidth: 650,
        to: 'con-top = vp-bottom',
        css: {
            opacity: '0',
            '-transition': 'opacity 2s'
        }
    },
    {
        minWidth: 900,
        from: 'con-center = vp-center +50con',
        css: {
            'left': '419px',
            'top': '-94px',
            '-transition': 'left .5s, top .5s',
            '-transition-duration': '1s'
        }
    },
    {
        minWidth: 900,
        to: 'con-center = vp-center +45con',
        css: {
            'left': '101px',
            'top': '48px',
            '-transition': 'left .5s, top .5s',
            '-transition-duration': '1s'
        }
    }], $('.caseItem__fourth_media_start'));

    $('.caseItem__fourth_media_4').scroolly([{
        minWidth: 650,
        from: 'con-top = vp-bottom -120',
        css: {
            opacity: '1',
            '-transition': 'opacity 2s'
        }
    },
    {
        minWidth: 650,
        to: 'con-top = vp-bottom',
        css: {
            opacity: '0',
            '-transition': 'opacity 2s'
        }
    },
    {
        minWidth: 900,
        from: 'con-center = vp-center +50con',
        css: {
            'left': '486px',
            'top': '-124px',
            '-transition': 'left .5s, top .5s',
            '-transition-duration': '1s'
        }
    },
    {
        minWidth: 900,
        to: 'con-center = vp-center +45con',
        css: {
            'left': '101px',
            'top': '48px',
            '-transition': 'left .5s, top .5s',
            '-transition-duration': '1s'
        }
    }], $('.caseItem__fourth_media_start'));

    $('.caseItem__fourth_media_l1, .caseItem__fourth_media_l2, .caseItem__fourth_media_l3').scroolly([{
        minWidth: 900,
        from: 'con-center = vp-center +50con',
        css: {
            opacity: '1',
            '-transition': 'opacity 8s'
        }
    },
    {
        minWidth: 900,
        to: 'con-center = vp-center +45con',
        css: {
            opacity: '0',
            '-transition': 'opacity .5s'
        }
    }], $('.caseItem__fourth_media_start'));

    $('.caseItem__fourth_text').scroolly([{
        minWidth: 1199,
        from: 'con-center = vp-center +100con',
        css: {
            opacity: '1',
            '-transition': 'opacity 2s'
        }
    },
    {
        minWidth: 1199,
        to: 'con-center = vp-center +110con',
        css: {
            opacity: '0',
            '-transition': 'opacity 2s'
        }
    },
    {
        maxWidth: 1200,
        minWidth: 650,
        from: 'el-top = vp-bottom -50',
        css: {
            opacity: '1',
            '-transition': 'opacity 2s'
        }
    },
    {
        maxWidth: 1200,
        minWidth: 650,
        to: 'el-top = vp-bottom -60',
        css: {
            opacity: '0',
            '-transition': 'opacity 2s'
        }
    }], $('.caseItem__fourth_media_start'));

    $('.caseItem__fifth_media_bg').scroolly([{
        minWidth: 650,
        from:'con-top = vp-bottom',
        to: 'con-bottom = vp-top',
        onScroll: function(element, rule, length){
            var progress = rule / length;

            element.css('right', $.scroolly.getTransitionFloatValue(-10, 10, progress)+'%');
        }
    }], $('.caseItem__fifth_media'));

    $('.caseItem__fifth_media_img').scroolly([{
        minWidth: 650,
        from: 'el-top = vp-center',
        css: {
            opacity: '1',
            '-transition': 'opacity 2s'
        }
    },
    {
        minWidth: 650,
        to: 'el-top -10el = vp-center',
        css: {
            opacity: '0',
            '-transition': 'opacity 2s'
        }
    }]);

    $('.caseItem__fifth_text').scroolly([{
        maxWidth: 1199,
        minWidth: 650,
        from: 'el-top = vp-bottom -130',
        css: {
            opacity: '1',
            '-transition': 'opacity 2s'
        }
    },
    {
        maxWidth: 1199,
        minWidth: 650,
        to: 'el-top = vp-bottom -140',
        css: {
            opacity: '0',
            '-transition': 'opacity 2s'
        }
    },
    {
        minWidth: 1200,
        from: 'con-top = vp-center -120',
        css: {
            opacity: '1',
            '-transition': 'opacity 2s'
        }
    },
    {
        minWidth: 1200,
        to: 'con-top = vp-center -130',
        css: {
            opacity: '0',
            '-transition': 'opacity 2s'
        }
    }], $('.caseItem__fifth_media_img'));

    $('.caseItem__sixth_in').scroolly([{
        minWidth: 650,
        from: 'con-top = vp-center +50',
        to: 'con-top = vp-center +40',
        css: {
            position: 'relative',
            display: 'block'
        }
    }], $('.caseItem__sixth'));

    $('.caseItem__sixth_media').scroolly([{
        minWidth: 650,
        from: 'el-top = vp-bottom -150',
        css: {
            opacity: 1,
            '-transition': 'opacity 2s'
        }
    },
    {
        minWidth: 650,
        to: 'el-top = vp-bottom -160',
        css: {
            opacity: 0,
            '-transition': 'opacity 2s'
        }
    }], $('.caseItem__sixth'));

    $('.caseItem__sixth_text, .caseItem__sixth_tabs').scroolly([{
        minWidth: 650,
        from: 'el-top = vp-bottom -180',
        css: {
            opacity: 1,
            '-transition': 'opacity 2s'
        },
        removeClass: 'nav-stacked'
    },
    {
        minWidth: 650,
        to: 'el-top = vp-bottom -190',
        css: {
            opacity: 0,
            '-transition': 'opacity 2s'
        }
    }], $('.caseItem__sixth'));

    $('#caseTabs').scroolly([{
        maxWidth: 1199,
        removeClass: 'nav-stacked'
    }]);


    $('.caseItem__seventh_media').scroolly([{
        minWidth: 650,
        from: 'con-top = vp-bottom',
        css: {
            opacity: '1',
            '-transition': 'opacity 2s'
        }
    },
    {
        minWidth: 650,
        to: 'con-top -10 = vp-center',
        css: {
            opacity: '0',
            '-transition': 'opacity 2s'
        }
    }], $('.caseItem__seventh_media_start'));

    $('.caseItem__seventh_media_right').scroolly([{
        minWidth: 650,
        from:'con-top + 10 = vp-bottom',
        to: 'con-bottom = vp-top',
        onScroll: function(element, rule, length){
            var progress = rule / length;

            element.css('margin-left', $.scroolly.getTransitionFloatValue(-200, -100, progress)+10);
        }
    }], $('.caseItem__seventh_media_start'));

    $('.caseItem__seventh_text').scroolly([{
        maxWidth: 1199,
        minWidth: 650,
        from: 'con-top = vp-bottom',
        css: {
            opacity: '1',
            '-transition': 'opacity 2s'
        }
    },
    {
        maxWidth: 1199,
        minWidth: 650,
        to: 'el-top = vp-center',
        css: {
            opacity: '0',
            '-transition': 'opacity 2s'
        }
    },
    {
        minWidth: 1200,
        from: 'con-top = vp-center -120',
        css: {
            opacity: '1',
            '-transition': 'opacity 2s'
        }
    },
    {
        minWidth: 1200,
        to: 'con-top = vp-center -130',
        css: {
            opacity: '0',
            '-transition': 'opacity 2s'
        }
    }], $('.caseItem__seventh_media_start'));

    $('.caseItem__eighth_media_bg').scroolly([{
        minWidth: 650,
        from:'con-top = vp-bottom',
        to: 'con-bottom = vp-top',
        onScroll: function(element, rule, length){
            var progress = rule / length;

            element.css('right', $.scroolly.getTransitionFloatValue(-10, 10, progress)+'%');
        }
    }], $('.caseItem__eighth_media'));

    $('.caseItem__eighth_media_img').scroolly([{
        minWidth: 650,
        from: 'el-top = vp-bottom',
        css: {
            opacity: '1',
            '-transition': 'opacity 2s'
        }
    },
    {
        minWidth: 650,
        to: 'el-top -10 = vp-bottom',
        css: {
            opacity: '0',
            '-transition': 'opacity 2s'
        }
    }]);

    $('.caseItem__eighth_text').scroolly([{
        maxWidth: 1199,
        minWidth: 650,
        from: 'el-top = vp-bottom -50',
        css: {
            opacity: 1,
            '-transition': 'opacity 2s'
        }
    },
    {
        maxWidth: 1199,
        minWidth: 650,
        to: 'el-top = vp-bottom -60',
        css: {
            opacity: 0,
            '-transition': 'opacity 2s'
        }
    },
    {
        minWidth: 1200,
        from: 'con-top = vp-center -70',
        css: {
            opacity: 1,
            '-transition': 'opacity 2s'
        }
    },
    {
        minWidth: 1200,
        to: 'con-top = vp-center -80',
        css: {
            opacity: 0,
            '-transition': 'opacity 2s'
        }
    }], $('.caseItem__eighth_media_img'));
});
function initMap() {
    var uluru = { lat: 35.9250977, lng: 50.8429529 },
        zoom = 3,
        width = (window.innerWidth > 0) ? window.innerWidth : screen.width;
    if (width < 400){
        zoom = 1;
    } else if (width < 800) {
        zoom = 2;
    }
    var map = new google.maps.Map(document.getElementById('map'), {
        zoom: zoom,
        center: uluru,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        disableDefaultUI: true,
        mapTypeControl: false,
        styles: [
            {
                "featureType": "all",
                "elementType": "labels.text.fill",
                "stylers": [
                    {
                        "saturation": 36
                    },
                    {
                        "color": "#000000"
                    },
                    {
                        "lightness": 40
                    }
                ]
            },
            {
                "featureType": "all",
                "elementType": "labels.text.stroke",
                "stylers": [
                    {
                        "visibility": "on"
                    },
                    {
                        "color": "#000000"
                    },
                    {
                        "lightness": 16
                    }
                ]
            },
            {
                "featureType": "all",
                "elementType": "labels.icon",
                "stylers": [
                    {
                        "visibility": "off"
                    }
                ]
            },
            {
                "featureType": "administrative",
                "elementType": "geometry.fill",
                "stylers": [
                    {
                        "color": "#000000"
                    },
                    {
                        "lightness": 20
                    }
                ]
            },
            {
                "featureType": "administrative",
                "elementType": "geometry.stroke",
                "stylers": [
                    {
                        "color": "#000000"
                    },
                    {
                        "lightness": 17
                    },
                    {
                        "weight": 1.2
                    }
                ]
            },
            {
                "featureType": "administrative",
                "elementType": "labels",
                "stylers": [
                    {
                        "visibility": "off"
                    }
                ]
            },
            {
                "featureType": "administrative.country",
                "elementType": "all",
                "stylers": [
                    {
                        "visibility": "simplified"
                    }
                ]
            },
            {
                "featureType": "administrative.country",
                "elementType": "geometry",
                "stylers": [
                    {
                        "visibility": "simplified"
                    }
                ]
            },
            {
                "featureType": "administrative.country",
                "elementType": "labels.text",
                "stylers": [
                    {
                        "visibility": "simplified"
                    }
                ]
            },
            {
                "featureType": "administrative.province",
                "elementType": "all",
                "stylers": [
                    {
                        "visibility": "off"
                    }
                ]
            },
            {
                "featureType": "administrative.locality",
                "elementType": "all",
                "stylers": [
                    {
                        "visibility": "simplified"
                    },
                    {
                        "saturation": "-100"
                    },
                    {
                        "lightness": "30"
                    }
                ]
            },
            {
                "featureType": "administrative.neighborhood",
                "elementType": "all",
                "stylers": [
                    {
                        "visibility": "off"
                    }
                ]
            },
            {
                "featureType": "administrative.land_parcel",
                "elementType": "all",
                "stylers": [
                    {
                        "visibility": "off"
                    }
                ]
            },
            {
                "featureType": "landscape",
                "elementType": "all",
                "stylers": [
                    {
                        "visibility": "simplified"
                    },
                    {
                        "gamma": "0.00"
                    },
                    {
                        "lightness": "74"
                    }
                ]
            },
            {
                "featureType": "landscape",
                "elementType": "geometry",
                "stylers": [
                    {
                        "color": "#000000"
                    },
                    {
                        "lightness": 20
                    }
                ]
            },
            {
                "featureType": "landscape.man_made",
                "elementType": "all",
                "stylers": [
                    {
                        "lightness": "3"
                    }
                ]
            },
            {
                "featureType": "poi",
                "elementType": "all",
                "stylers": [
                    {
                        "visibility": "off"
                    }
                ]
            },
            {
                "featureType": "poi",
                "elementType": "geometry",
                "stylers": [
                    {
                        "color": "#000000"
                    },
                    {
                        "lightness": 21
                    }
                ]
            },
            {
                "featureType": "road",
                "elementType": "geometry",
                "stylers": [
                    {
                        "visibility": "simplified"
                    }
                ]
            },
            {
                "featureType": "road.highway",
                "elementType": "geometry.fill",
                "stylers": [
                    {
                        "color": "#000000"
                    },
                    {
                        "lightness": 17
                    }
                ]
            },
            {
                "featureType": "road.highway",
                "elementType": "geometry.stroke",
                "stylers": [
                    {
                        "color": "#000000"
                    },
                    {
                        "lightness": 29
                    },
                    {
                        "weight": 0.2
                    }
                ]
            },
            {
                "featureType": "road.arterial",
                "elementType": "geometry",
                "stylers": [
                    {
                        "color": "#000000"
                    },
                    {
                        "lightness": 18
                    }
                ]
            },
            {
                "featureType": "road.local",
                "elementType": "geometry",
                "stylers": [
                    {
                        "color": "#000000"
                    },
                    {
                        "lightness": 16
                    }
                ]
            },
            {
                "featureType": "transit",
                "elementType": "geometry",
                "stylers": [
                    {
                        "color": "#000000"
                    },
                    {
                        "lightness": 19
                    }
                ]
            },
            {
                "featureType": "water",
                "elementType": "geometry",
                "stylers": [
                    {
                        "color": "#000000"
                    },
                    {
                        "lightness": 17
                    }
                ]
            }
        ]
    });
    var image = "/images/combined-shape.png";
    setMarker(51.5161254, -0.145361);
    setMarker(50.4655589, 30.5086638);
    setMarker(43.23217, 76.949786);
    setMarker(10.7759279, 106.6967906);
    function setMarker(lat, ing) {
        new google.maps.Marker({
            position: { lat: lat, lng: ing },
            map: map,
            icon:image
        });
    }
};
$(document).ready(function() {

    $('.formInhomeBTN').click(function () { // ловим клик по ссылке с классом go_to
        var scroll_el = $(this).attr('href'); // возьмем содержимое атрибута href, должен быть селектором, т.е. например начинаться с # или .
        if ($(scroll_el).length != 0) { // проверим существование элемента чтобы избежать ошибки
            $('html, body').animate({scrollTop: $(scroll_el).offset().top}, 1000); // анимируем скроолинг к
            // элементу scroll_el
        }
        return false; // выключаем стандартное действие
    });

    $(function () {
        $('.case__item_img, .blog__item_img').height($('.case__item_img, .blog__item_img').width() / 1);

        $(window).resize(function () {
            $('.case__item_img, .blog__item_img').height($('.case__item_img, .blog__item_img').width() / 1);
        });
    });
    $('.atb-content').each(function () {
        var highestBox = 0;
        $('.atb-photo', this).each(function () {
            if ($(this).height() > highestBox) {
                highestBox = $(this).height();
            }
        });
        $('.atb-photo', this).height(highestBox);
    });

    $('.blog__items').each(function () {
        var highestBox = 0;
        $('.blog__item__cntnt', this).each(function () {
            if ($(this).height() > highestBox) {
                highestBox = $(this).height();
            }
        });
        $('.blog__item__cntnt', this).height(highestBox);
    });

    var loc = window.location.hash.replace("#", "");
    if (loc != "") {
        tt = $('#' + loc);
        setTimeout(function () {
        tt.click();
        }, 2000);
    }

    var widthViewport = $(window).width();

    if (widthViewport <= 426) {
        $('#view360Block0,#view360Block1,#view360Block2,#view360Block3,#view360Block4,#view360Block5#view360Block7,#view360Block8,#view360Block9,#view360Block10').parent().css({'bottom':'0', 'height':'100vh'});
    };
});
