 (function ($) {
    var popupNo = 0;
    $.fn.ecmsPopup = function (options, method) {
        var $this = this;

        var settings = $.extend({
            hasoverlay: true,
            overlayclose: true,
            overlayopacity: 0.4,
            iscentered: true,
            closebutton: true,
            smallclosebutton: false,
            html: ''
        }, options);

        var handlers = function () {
            $('.page-overlay').bind('click', function () {
                methods.closebyoverlay();
            });

            $('.popup-closebtn').bind('click', function () {
                methods.close();
            });

            $(window).on('resize', function () {
                methods.resize();
            });

            $(window).on('scroll', function () {
                methods.resize();
            });
        }

        var methods = {
            init: function () {
                if (!$('.page-overlay').length)
                    $('body').append('<div class="page-overlay"></div>');

                var hasHtml = settings.html != '';
                popupNo = hasHtml ? popupNo + 1 : popupNo;
                var popupIdIfHtml = 'page-popup' + popupNo;
                var result = $this.each(function () {
                    var popupId = $(this).attr('popupid');
                    $(this).css('cursor', 'pointer');

                    if (hasHtml || (typeof popupId !== 'undefined' && popupId != '' && $('#' + popupId).length)) {
                        var $popup = hasHtml ? $('<div/>').html(settings.html).wrapAll('<div></div>').attr('id', popupIdIfHtml) : $('#' + $(this).attr('popupid'));

                        if (hasHtml && !$('#' + popupIdIfHtml).length) {
                            $('body').append($popup);
                        }

                        if (hasHtml)
                            $(this).attr('popupid', popupIdIfHtml);

                        $popup.addClass('page-popup').data('ecmsPopup', { settings: settings });

                        if (settings.closebutton)
                            $popup.append(settings.smallclosebutton ? '<a class="popup-closebtn popup-small-closebtn"><img src="/uploads/close.svg" data-uk-svg></a>' : '<a class="popup-closebtn"><img src="/uploads/close.svg" data-uk-svg></a>');

                        $(this).bind('click', function (event) {
                            event.preventDefault();
                            methods.click(this);
                        });
                    }
                    else {
                        $(this).css('cursor', 'default');
                    }
                });

                handlers();

                return result;
            },
            click: function (button) {
                if (button == 'click')
                    button = $this.first();

                var $popup = $('#' + $(button).attr('popupid'));
                $(button).addClass('active');

                if (settings.hasoverlay)
                    $('.page-overlay').css({
                        'filter': 'alpha(opacity=' + settings.overlayopacity * 100 + ')',
                        'opacity': settings.overlayopacity,
                        'cursor': settings.overlayclose ? 'pointer' : 'default'
                    }).fadeIn();

                $popup.fadeIn();
                methods.build($popup);
            },
            close: function () {
                $('.page-popup, .page-overlay').fadeOut();
                $this.removeClass('active');
                methods.pausevideo($(this));
            },
            closebyoverlay: function () {
                $('.page-popup').each(function () {
                    if ($(this).is(':visible') && $(this).data('ecmsPopup').settings.overlayclose)
                    {
                        $(this).fadeOut();
                        $('.page-overlay').fadeOut();
                        $this.removeClass('active');
                        methods.pausevideo($(this));
                    }
                });
            },
            resize: function () {
                var $popup = $('.page-popup');
                if (!$popup.length)
                    return;

                $popup.each(function() {
                    if ($(this).css('display') != 'none')
                        methods.build($(this));
                });
            },
            pausevideo: function (obj) {
                $('.video', obj).each(function () {
                    $(this).get(0).pause();
                });
            },
            build: function (popup) {
                var localSettings = popup.data('ecmsPopup').settings;

                if (localSettings.iscentered) {
                    popup.addClass('fixed');
                }
            }
        }

        if (methods[method]) {
            return methods[method].apply(this, Array.prototype.slice.call(arguments, 1));
        } else if (typeof method === 'object' || !method) {
            return methods.init.apply(this, arguments);
        } else {
            $.error('Method ' + method + ' does not exist for jQuery.ecmsPopup');
        }
    }
})(jQuery);
