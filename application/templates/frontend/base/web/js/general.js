﻿var Estate = Estate || {};
Estate.UI = (function () {
    var ui = {};

    ui.init = function (isForPopupEditor) {
        ui.Visualization.init();
        ui.Carousel.init();
        ui.WorksBlockVideo.init();
        ui.MobileMenu.init();
        ui.TouchDevicesAutoHover.init();
        ui.TeamBlock.init();
        ui.GetStart.init();
        ui.GetStartBack.init();
        ui.GoTop.init();
        ui.WorkListMobile.init();
        ui.TripleCarousel.init();
        ui.WorkAnimation.init();
        ui.WorkVideoPopup.init();
    };

    ui.WorksBlockVideo = {
        init: function () {
            $('.works-block .square-content').hover(
                function () {
                    var $video = $(this).find('.video');
                    if ($video.length) {
                        $video.get(0).play();
                    }
                },
                function () {
                    var $video = $(this).find('.video');
                    if ($video.length) {
                        $video.get(0).pause();
                    }
                }
            );
        }
    };

    ui.WorkVideoPopup = {
        init: function () {
            var $link = $('.video-popup-link');
            if (!$link.length) {
                return;
            }

            $('.video-popup-link').ecmsPopup();
            $('.wbb-item').click(function () {
                var source = $(this).attr('data-video');
                if (typeof source == 'undefined' || poster == '') {
                    return;
                }

                var poster = $(this).attr('data-poster');
                var $video = $('.video-popup video');
                $video.find('source').attr('src', source);
                if (typeof poster != 'undefined' && poster != '') {
                    $video.attr('poster', poster);
                }

                $video.get(0).load();
                $('.video-popup-link').click();
            });
        }


    };

    ui.Visualization = {
        loaderInterval: null,
        isFirstImageLoaded: false,
        init: function () {
            var imagePath = $('.top-home-slider:not(.portfolio-slider) .th-slider-item:first').css('background-image');
            if (typeof imagePath != 'undefined' && imagePath != '') {
                $('<img />').attr('src', imagePath.replace('url(', '').replace(')', '').replace('"', '').replace('"', '')).on('load', function () {
                    ui.Visualization.isFirstImageLoaded = true;
                });
            }
            else {
                $('.mobile-menu-btn, .header-loader, .logo, .h-top-menu').addClass('visible');
                return;
            }
            var $loader = $('.header-loader');
            var interval = window.outerWidth / 10;
            var counter = 2;
            $loader.animate({ 'width': interval }, 320, function () {
                counter++;
            });
            ui.Visualization.loaderInterval = setInterval(function () {
                if (!ui.Visualization.isFirstImageLoaded && counter <= 10) {
                    var newWidth = interval * counter;
                    $loader.animate({ 'width': newWidth > window.outerWidth ? window.outerWidth : newWidth }, 320, function () {
                        counter++;
                    });
                }
                else {
                    clearInterval(ui.Visualization.loaderInterval);
                    $loader.animate({ 'width': window.outerWidth }, 800, function () {
                        $loader.addClass('finished');
                        $('.mobile-menu-btn, .logo, .h-top-menu').addClass('finished');
                        var $video = $('.top-home-slider:not(.portfolio-slider) .video');
                        if ($video.length) {
                            $video.get(0).play();
                        }
                    });
                }
            }, 400);
        }
    };

    ui.TouchDevicesAutoHover = {
        init: function () {
            if ($('html').hasClass('touch-device')) {
                var $items = $('.works-block .square-box, .works-block .square-content');
                $(window).on('scroll', function () {
                    ui.TouchDevicesAutoHover.checkInView($items);
                });

                $(window).on('resize', function () {
                    ui.TouchDevicesAutoHover.checkInView($items);
                });

                $(window).on('orientationchange', function () {
                    ui.TouchDevicesAutoHover.checkInView($items);
                });
            }
        },
        checkInView: function (items) {
            items.each(function () {
                if (ui.Carousel.isElementInView($(this), $(this).height() > window.innerHeight ? false : true)) {
                    $(this).addClass('hover');
                }
                else {
                    $(this).removeClass('hover');
                }
            });
        }
    }

    ui.Carousel = {
        videoResizeTimeout: null,
        init: function () {
            var $slider = $('.top-home-slider');
            if ($slider.length) {
               /* $slider.slick({
                    dots: true,
                    arrows: false,
                    useTransform: true,
                    infinite: true,
                    speed: 700,
                    autoplay: true,
                    autoplaySpeed: 5000
                });*/

                $slider.each(function () {
                    ui.Carousel.setVideoAspectRatio($(this));
                });

                $slider.on('afterChange', function (event, slick, currentSlide, nextSlide) {
                    var $this = $(this);
                    slick.slickSetOption("autoplay", true, false)
                    $('.video', $this).each(function () {
                        $(this).get(0).pause();
                    });

                    var $video = $this.find('[data-slick-index="' + currentSlide + '"] .video');
                    if ($video.length) {
                        $video.get(0).play();
                        slick.slickSetOption("autoplay", false, false);
                        $video.on('ended', function () {
                            slick.slickSetOption("autoplay", true, true);
                        });
                    }
                });

                $('.video-play-button').click(function () {
                    var $video = $(this).prev();
                    $video.attr('controls', 'controls');
                    $video.get(0).play();
                    $(this).addClass('none');
                });

                $('.video').click(function () {
                    var $btn = $(this).parent().find('.video-play-button');
                    if ($btn.length && $btn.hasClass('none')) {
                        $btn.removeClass('none');
                        $(this).get(0).pause();
                    }
                });

                $(window).on('scroll', function () {
                    $slider.each(function () {
                        ui.Carousel.stopVideoOnScroll($(this));
                    });
                });

                $(window).on('resize', function () {
                    var dd = $(window).width();
                    var cur = 1012;
                    if (dd > cur) {
                        dd = dd - 337;
                        $('.slick-track').css('width',dd+'px');
                        $('.slick-slider .th-slider-item-wrapper').css('width',dd+'px');
                    } else {
                        $('.slick-track').css('width',dd+'px');
                        $('.slick-slider .th-slider-item-wrapper').css('width',dd+'px');
                    }
                    ui.Carousel.setVideoAspectRatio();
                    $slider.each(function () {
                        ui.Carousel.stopVideoOnScroll($(this));
                    });
                });

                $(window).on('orientationchange', function () {
                    ui.Carousel.setVideoAspectRatio();
                    $slider.each(function () {
                        ui.Carousel.stopVideoOnScroll($(this));
                    });
                });
            }
        },
        stopVideoOnScroll: function (obj) {
            if (obj.length && !ui.Carousel.isElementInView(obj, false)) {
               $('.video', obj).each(function () {
                   $(this).get(0).pause();
               });
            }
            else {
               var $currentVideo = $('.slick-active .video', obj);
               var $btn = $currentVideo.parent().find('.video-play-button');
               if ($btn.length && !$btn.hasClass('none')) {
                   return;
               }

               if ($currentVideo.length) {
                   $currentVideo.get(0).play();
               }
           }
        },
        setVideoAspectRatio: function () {
            clearTimeout(ui.Carousel.videoResizeTimeout);
            ui.Carousel.videoResizeTimeout = setTimeout(function () {
                $('.video').each(function () {
                    var $wrapper = $(this).parent();
                    if ($wrapper.outerWidth() < $wrapper.outerHeight() * 1.778) {
                        $(this).addClass('portrait');
                    }
                    else {
                        $(this).removeClass('portrait');
                    }
                });
            }, 100);
        },
        isElementInView: function (element, fullyInView) {
            var pageTop = $(window).scrollTop();
            var pageBottom = pageTop + $(window).height();
            var elementTop = $(element).offset().top;
            var elementBottom = elementTop + $(element).height();

            if (fullyInView === true) {
                return ((pageTop < elementTop) && (pageBottom > elementBottom));
            } else {
                return ((elementTop <= pageBottom) && (elementBottom >= pageTop));
            }
        }
    };

    ui.MobileMenu = {
        scrollMenuTimeout: null,
        init: function () {
            $('.header-loader').click(function () {
                if (window.outerWidth < 901) {
                    $(this).toggleClass('opened');
                }
            });

            $(window).on('orientationchange', function () {
                ui.MobileMenu.checkForRemovingStickyHeader($('header'));

                $('.h-top-menu').addClass('none');
                $('.header-loader').removeClass('opened');
                setTimeout(function () {
                    $('.h-top-menu').removeClass('none');
                }, 300);
            });

            $(window).on('scroll', function () {
                clearTimeout(ui.MobileMenu.scrollMenuTimeout);
                ui.MobileMenu.scrollMenuTimeout = setTimeout(function () {
                    var $header = $('header');
                    if (ui.MobileMenu.checkForRemovingStickyHeader($header)) {
                        return;
                    }

                    if ($header.is(':animated')) {
                        return;
                    }

                    if ($(window).scrollTop() > window.outerHeight) {
                        if (!$header.hasClass('fixed')) {
                            $header.addClass('fixed').css({ 'top': -84 });
                            $header.stop().animate({ 'top': 0 }, 400);
                        }
                    }
                    else {
                        if ($header.hasClass('fixed')) {
                            if ($(window).scrollTop() > 84) {
                                $header.stop().animate({ 'top': -84 }, 400, function () { $header.removeClass('fixed').css({ 'top': 0 }); });
                            }
                            else {
                                $header.removeClass('fixed').css({ 'top': 0 });
                            }
                        }
                    }
                }, 100);
            });
        },
        checkForRemovingStickyHeader: function (header) {
           var wwidth = window.outerWidth;
           var wheight = window.outerHeight;
           var isLandscapeMobile = wwidth > wheight && wwidth > 639 && wwidth < 768;
           if (isLandscapeMobile) {
               header.removeClass('fixed').css({ 'top': 0 });
           }

           return isLandscapeMobile;
        }
    };

    ui.TeamBlock = {
        teamBlockTimeout: null,
        teamBlockTimeout2: null,
        init: function () {
            $('.team-block-face').hover(
                function () {
                    if ($(this).find('img').length > 1) {
                        ui.TeamBlock.doAnimate();
                    }
                },
                function () {
                    if ($(this).find('img').length > 1) {
                        $('.team-block-face img:animated').stop().fadeIn().css({ 'opacity': 1 });
                        clearTimeout(ui.TeamBlock.teamBlockTimeout);
                        clearTimeout(ui.TeamBlock.teamBlockTimeout2);
                    }
                }
            );
        },
        doAnimate: function () {
            clearTimeout(ui.TeamBlock.teamBlockTimeout);
            clearTimeout(ui.TeamBlock.teamBlockTimeout2);
            var $wrapper = $('.team-block-face');
            var $currentImage = $wrapper.find('img:visible');
            $currentImage.fadeOut(500);
            ui.TeamBlock.teamBlockTimeout = setTimeout(function () {
                if ($currentImage.is(':last-child')) {
                    $wrapper.find('img:first').fadeIn(500);
                }
                else {
                    $currentImage.next().fadeIn(500);
                }

                ui.TeamBlock.teamBlockTimeout2 = setTimeout(function () {
                    ui.TeamBlock.doAnimate();
                }, 1200);
            }, 500);
        }
    };

    ui.DetectIE = function () {
        if (navigator.userAgent.indexOf('MSIE') != -1)
            var detectIEregexp = /MSIE (\d+\.\d+);/
        else
            var detectIEregexp = /Trident.*rv[ :]*(\d+\.\d+)/

        if (detectIEregexp.test(navigator.userAgent)) {
            var ieversion = new Number(RegExp.$1)
            if (ieversion >= 12)
                $('html').addClass('ie ie12');
            else if (ieversion >= 11)
                $('html').addClass('ie ie11');
            else if (ieversion >= 10)
                $('html').addClass('ie ie10');
        }
    };

    ui.DetectTouch = function () {
        if ('ontouchstart' in window || navigator.maxTouchPoints) {
            $('html').addClass('touch-device');
        }
    };

    ui.GetStart = {
        init: function () {
            var content = $('.pgb-content'),
                wrapper = $('.portfolio-get-started-block'),
                form = $('.pgb-form'),
                contentHeight = content.outerHeight(),
                formHeight = form.outerHeight();
                content.css('height', contentHeight);
                wrapper.css('height', contentHeight);
                form.css('top', contentHeight);

            $('.pgb-button').click(function () {
                contentHeight = content.outerHeight();
                formHeight = form.outerHeight();
                wrapper.css('height', formHeight);
                content.css('height', formHeight);
                form.css('top', formHeight);
                setTimeout(function () {
                    content.css('top', -formHeight);
                    form.css('top', '0');
                }, 750);
            });
        }
    };

    ui.GetStartBack = {
        init: function () {
            var content = $('.pgb-content'),
                form = $('.pgb-form'),
                wrapper = $('.portfolio-get-started-block'),
                contentHeight = content.outerHeight(),
                formHeight = form.outerHeight(),
                formTop, contentTop;

            $(window).on('orientationchange', function (event) {
                form.css('height', 'auto');
                content.css('height', 'auto');
                contentHeight = content.outerHeight();
                formHeight = form.outerHeight();
                form.css('height', formHeight);
                if (form.length && content.length) {
                    formTop = form.position().top;
                    contentTop = content.position().top;
                }

                if (formTop == 0) {
                    wrapper.css('height', formHeight);
                    content.css({'height': formHeight, 'top': -formHeight});
                } else if (contentTop == 0) {
                    wrapper.css('height', contentHeight);
                    content.css('height', contentHeight);
                }
            });

            $('.pgb-white-line-wrapper').click(function () {
                content.css('top', 0);
                form.css('top', formHeight);
                setTimeout(function () {
                    content.css('height', contentHeight);
                    wrapper.css('height', contentHeight);
                    form.css('top', contentHeight);
                }, 750);
            });
        }
    };

    ui.WorkListMobile = {
        init: function () {
            $(window).on('resize', function () {
                ui.WorkListMobile.workListDisplay();
            });

            $(window).on('orientationchange', function () {
                ui.WorkListMobile.workListDisplay();
            });

            ui.WorkListMobile.workListDisplay();

            $('.wpb-title-wrapper').click(function () {
                var openClose = $(this).find('.wpb-title-text'),
                    ul = $(this).parent().find('ul');

                if (openClose.hasClass('wpb-title-plus')) {
                    ul.stop();
                    ul.show(500);
                    openClose.removeClass('wpb-title-plus').addClass('wpb-title-minus');
                } else {
                    ul.stop();
                    ul.hide(500);
                    openClose.removeClass('wpb-title-minus').addClass('wpb-title-plus')
                }
            });
        },
        workListDisplay: function () {
            var width = (window.innerWidth > 0) ? window.innerWidth : screen.width;
            $('.wpb-title-text').each(function () {
                var openClose = $(this),
                    ul = $(this).parent().parent().find('ul');

                if (width <= 639 && openClose.hasClass('wpb-title-minus')){
                    ul.css('display', 'block');
                } else if (width <= 639 && openClose.hasClass('wpb-title-plus')){
                    ul.css('display', 'none');
                } else {
                    ul.css('display', 'block');
                }
            });

        }
    };

    ui.GoTop = {
        init: function () {
            $('.go-top').click(function () {
                $('html, body').animate({ scrollTop: 0 }, 800);
                return false;
            });
        }
    };
    
    ui.TripleCarousel = {
          init: function () {
              $('#wrb-slider').slick({
                  arrows:false,
                  infinite: true,
                  dots: true,
                  dotsClass: 'work-dots',
                  slidesToShow: 3,
                  slidesToScroll: 3,
                  responsive:[
                      {
                          breakpoint: 1024,
                          settings:{
                              slidesToShow: 2,
                              slidesToScroll: 2
                          }
                      },
                      {
                          breakpoint: 768,
                          settings:{
                              slidesToShow: 1,
                              slidesToScroll: 1
                          }
                      }
                  ]
              });
          }
    };
    ui.WorkAnimation = {
            init: function () {
                $( ".wrb-slider-block" ).hover(
                function() {
                    var obg = $( this ).find('.wrb-slider-text');
                    obg.css('height', 'auto');
                    var height = $( this ).find('.wrb-slider-text').height();
                    obg.css('height', 0);
                    obg.stop().animate({height: height}, 650);
                }, function() {
                    $( this ).find('.wrb-slider-text').stop().animate({height: 0}, 650);
                });
            }
    };

    return ui;
}());

$(function () {
    $('html').removeClass('no-js'); /*To detect javascript*/
    Estate.UI.DetectIE();
    Estate.UI.DetectTouch();
    Estate.UI.init();
});

