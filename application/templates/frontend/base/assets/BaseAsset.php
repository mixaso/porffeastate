<?php
/**
 * @package    oakcms
 * @author     Hryvinskyi Volodymyr <script@email.ua>
 * @copyright  Copyright (c) 2015 - 2017. Hryvinskyi Volodymyr
 * @version    0.0.1-beta.0.1
 */

namespace app\templates\frontend\base\assets;

use yii\web\AssetBundle;
use yii\web\View;

class BaseAsset extends AssetBundle
{
    public $sourcePath = '@app/templates/frontend/base/web/';
    public $basePath = '@app/templates/frontend/base/web/';

    public $css = [
        'css/slick.css',
        'css/general.css',
        'css/mobile.css',
    ];

    public $js = [
        'js/jquery.scroolly.min.js',
        'js/slick.js',
        'js/general.js',
        'js/ecms-popup.js',
        'js/script.js',
    ];

    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapPluginAsset',
        'app\assets\FontAwesome',
        'app\templates\frontend\base\assets\WowAsset',
    ];

    public $jsOptions = [
        'position' => View::POS_END
    ];

    //    disableCache
    public $publishOptions = [
        'forceCopy'=>true,
    ];
}
