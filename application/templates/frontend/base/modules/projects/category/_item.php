<?php
/**
 * Created by Vladimir Hryvinskyy.
 * Site: http://codice.in.ua/
 * Date: 21.11.2016
 * Project: kardamon_blog
 * File name: _item.php
 *
 * @var $model \app\modules\content\models\ContentArticles
 */

use yii\helpers\Url;
use yii\bootstrap\Html;

?>
<a href="<?= Url::to(['/projects/project/view', 'catslug' => $model->category->slug, 'slug' => $model->slug]) ?>">
    <div class="blog__item_img" style="background: url('<?= $model->getThumbUploadUrl('image') ?>') center; background-size: cover;">
    </div>
    <div class="blog__item__cntnt">
        <div class="blog__item__cntnt_ttl">
            <span>
                <?= $model->title ?>
            </span>
        </div>
        <div class="blog__item__cntnt_content">
            <?= $model->description ?>
        </div>
    </div>
</a>
