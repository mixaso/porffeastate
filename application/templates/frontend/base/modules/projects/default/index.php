<?php
/**
 * @package    oakcms
 * @author     Hryvinskyi Volodymyr <script@email.ua>
 * @copyright  Copyright (c) 2015 - 2017. Hryvinskyi Volodymyr
 * @version    0.0.1
 */
use app\modules\projects\models\ProjectsCategory;

$categories = ProjectsCategory::find()->published()->all();
?>
<div class="container-fluid" data-ng-init="pageClass = 'projects-all'; animateClass = 'fadeIn'">
    <div class="row">
        <div class="col-sm-9 col-sm-push-3 slideInRight animated">
            <div class="projects-all__page">
                <div compile="content"></div>
                [block position='projects_all_content']
            </div>
        </div>

        <div class="col-sm-3 col-sm-pull-9 slideInLeft animated">
            <div class="sidebar-menu">
                <ul data-active-link>
                    <li class="active">
                        <a active-link href="<?= \yii\helpers\Url::to(['/projects/default/index']) ?>">
                            <?= Yii::t('projects', 'All') ?>
                        </a>
                    </li>
                    <?php foreach ($categories as $category): ?>
                        <li><a active-link href="<?= \yii\helpers\Url::to(['/projects/category/view', 'slug' => $category->slug]) ?>"><?= $category->title ?></a></li>
                    <?endforeach;?>
                </ul>
            </div>
        </div>
    </div>
</div>
