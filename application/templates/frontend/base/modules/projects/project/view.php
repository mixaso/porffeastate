<?php
/**
 * @package    oakcms
 * @author     Hryvinskyi Volodymyr <script@email.ua>
 * @copyright  Copyright (c) 2015 - 2017. Hryvinskyi Volodymyr
 * @version    0.0.1
 */
?>

<div class="container-fluid" data-ng-init="pageClass = 'projects-single'; animateClass = 'fadeIn'">
    <div class="row">
        <div class="col-sm-9 col-sm-push-3 slideInRight animated">
            <div class="projects-single__page" data-height-full>
                <div class="row">
                    <div class="col-sm-7">
                        <div class="row">
                            <div class="image" data-height-full>
                                <img src="{{project.image}}" alt="" class="img-responsive center-block">
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-5">
                        <div class="projects-single__content" data-height-full>
                            <h2>{{project.title}}</h2>
                            <div class="scroll" data-ng-scrollbar rebuild-on="rebuild:me">
                                <div compile="content"></div>

                                <div class="download">
                                    <a href="{{project.download_link}}" target="_blank">
                                        <?= Yii::t('projects', 'Download') ?> PDF
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-sm-3 col-sm-pull-9">
            <div class="sidebar-menu fadeIn animated">
                <ul data-active-link>
                    <li class="active">
                        <a active-link href="{{project.linkAbout}}"><?= Yii::t('projects', 'About') ?></a>
                    </li>
                    <li>
                        <a active-link href="{{project.linkLocation}}"><?= Yii::t('projects', 'Location') ?></a>
                    </li>
                    <li data-ng-if="project.facilities.length > 0">
                        <a active-link href="{{project.linkFacilities}}"><?= Yii::t('projects', 'Facilities') ?></a>
                    </li>
                    <li data-ng-if="project.gallery.length > 0">
                        <a active-link href="{{project.linkGallery}}"><?= Yii::t('projects', 'Gallery') ?></a>
                    </li>
                </ul>
            </div>

            <a href="{{project.category.link}}" class="back">
                <?= Yii::t('content', '&lt; Back to') ?> {{project.category.title}}
            </a>
        </div>
    </div>
</div>
