<?php
/**
 * @package    oakcms
 * @author     Hryvinskyi Volodymyr <script@email.ua>
 * @copyright  Copyright (c) 2015 - 2017. Hryvinskyi Volodymyr
 * @version    0.0.1
 */

?>

<div class="container-fluid" data-ng-init="pageClass = 'projects-single'; animateClass = 'fadeIn'">
    <div class="row">
        <div class="col-sm-9 col-sm-push-3 slideInRight animated">
            <div class="projects-single__page"> 
                <div class="row">
                    <div data-ng-if="project.facilities.length > 0">
                        <div data-double-carousel-repeat>
                            <div class="double-carousel facilities-carousel js-double-carousel-1">
                                <div
                                    data-double-count="1"
                                    data-options='{"items":"3", "margin":0, "mouseDrag":false}'
                                    class="owl-carousel project-gallery"
                                >
                                    <a
                                        data-owl-carousel-item
                                        data-ng-repeat="item in ::project.facilities"
                                        class="item"
                                        href="{{::item.link}}"
                                        rel="gallery_2"
                                    >
                                        <div class="item-image" style="background-image: url('{{::item.image_small}}')"></div>
                                        <div class="item-data">
                                            <div class="item-title">{{::item.title}}</div>
                                        </div>
                                    </a>
                                </div>
                                <div class="owl-nav">
                                    <div class="owl-prev js-double-prev-2" data-ng-if="project.facilities.length > 3">prev</div>
                                    <div class="owl-next js-double-next-2" data-ng-if="project.facilities.length > 3 && project.facilities.length < 6">next</div>
                                </div>
                            </div>

                            <div class="double-carousel facilities-carousel js-double-carousel-2" ng-if="project.facilities.length > 5">
                                <div
                                    data-double-count="2"
                                    data-options='{"items":"3", "margin":0, "mouseDrag":false}'
                                    class="owl-carousel project-gallery"
                                >
                                    <a
                                        data-owl-carousel-item
                                        data-ng-repeat="item in ::project.facilities"
                                        class="item"
                                        href="{{::item.link}}"
                                        rel="gallery_2"
                                    >
                                        <div class="item-data">
                                            <div class="item-image" style="background-image: url('{{::item.image_small}}')"></div>
                                            <div class="item-title">{{::item.title}}</div>
                                        </div>
                                    </a>
                                </div>
                                <div class="owl-nav">
                                    <div class="owl-next js-double-next-2">next</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-sm-3 col-sm-pull-9">
            <div class="sidebar-menu fadeIn animated">
                <ul data-active-link>
                    <li class="active">
                        <a active-link href="{{project.linkAbout}}"><?= Yii::t('projects', 'About') ?></a>
                    </li>
                    <li>
                        <a active-link href="{{project.linkLocation}}"><?= Yii::t('projects', 'Location') ?></a>
                    </li>
                    <li data-ng-if="project.facilities.length > 0">
                        <a active-link href="{{project.linkFacilities}}"><?= Yii::t('projects', 'Facilities') ?></a>
                    </li>
                    <li data-ng-if="project.gallery.length > 0">
                        <a active-link href="{{project.linkGallery}}"><?= Yii::t('projects', 'Gallery') ?></a>
                    </li>
                </ul>
            </div>

            <a href="{{project.category.link}}" class="back">
                <?= Yii::t('content', '&lt; Back to') ?> {{project.category.title}}
            </a>
        </div>
    </div>
</div>
