<?php
/**
 * @package    oakcms
 * @author     Hryvinskyi Volodymyr <script@email.ua>
 * @copyright  Copyright (c) 2015 - 2017. Hryvinskyi Volodymyr
 * @version    0.0.1
 */
?>
<div class="container-fluid" data-ng-init="pageClass = 'projects-single'; animateClass = 'fadeIn'">
    <div class="row">
        <div class="col-sm-9 col-sm-push-3 slideInRight animated">
            <div class="projects-single__page">
                <div class="row">
                    <div data-ng-if="project.gallery.length > 0">
                        <div data-double-carousel-repeat>
                            <div class="double-carousel js-double-carousel-1">
                                <div
                                    data-double-count="1"
                                    data-options='{"items":"3", "margin":0, "mouseDrag":false,"responsive":{"0":{"items":1},"768":{"items":2},"992":{"items":3}}}'
                                    class="owl-carousel project-gallery"
                                >
                                    <a
                                        data-owl-carousel-item
                                        data-fancybox
                                        data-ng-repeat="item in ::project.gallery"
                                        class="item"
                                        style="background-image: url('{{::item.imageUrl}}')"
                                        href="{{::item.imageUrl}}"
                                        rel="gallery"
                                    >

                                    </a>
                                </div>
                                <div class="owl-nav">
                                    <div class="owl-prev js-double-prev-2" data-ng-if="project.gallery.length > 3">prev</div>
                                    <div class="owl-next js-double-next-2" data-ng-if="project.gallery.length > 3 && project.gallery.length < 6">next</div>
                                </div>
                            </div>

                            <div class="double-carousel js-double-carousel-2" ng-if="project.gallery.length > 5">
                                <div
                                    data-double-count="2"
                                    data-options='{"items":"3", "margin":0, "mouseDrag":false,"responsive":{"0":{"items":1},"768":{"items":2},"992":{"items":3}}}'
                                    class="owl-carousel project-gallery"
                                >
                                    <a
                                        data-owl-carousel-item
                                        data-fancybox
                                        data-ng-repeat="item in ::project.gallery"
                                        class="item"
                                        style="background-image: url('{{::item.imageUrl}}')"
                                        href="{{::item.imageUrl}}"
                                        rel="gallery_2"
                                    >

                                    </a>
                                </div>
                                <div class="owl-nav">
                                    <div class="owl-next js-double-next-2">next</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-sm-3 col-sm-pull-9">
            <div class="sidebar-menu fadeIn animated">
                <ul data-active-link>
                    <li class="active">
                        <a active-link href="{{project.linkAbout}}"><?= Yii::t('projects', 'About') ?></a>
                    </li>
                    <li>
                        <a active-link href="{{project.linkLocation}}"><?= Yii::t('projects', 'Location') ?></a>
                    </li>
                    <li data-ng-if="project.facilities.length > 0">
                        <a active-link href="{{project.linkFacilities}}"><?= Yii::t('projects', 'Facilities') ?></a>
                    </li>
                    <li>
                        <a active-link href="{{project.linkGallery}}"><?= Yii::t('projects', 'Gallery') ?></a>
                    </li>
                </ul>
            </div>

            <a href="{{project.category.link}}" class="back">
                <?= Yii::t('content', '&lt; Back to') ?> {{project.category.title}}
            </a>
        </div>
    </div>
</div>
