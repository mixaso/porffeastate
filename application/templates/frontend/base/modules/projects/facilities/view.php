<?php
/**
 * @package    oakcms
 * @author     Hryvinskyi Volodymyr <script@email.ua>
 * @copyright  Copyright (c) 2015 - 2017. Hryvinskyi Volodymyr
 * @version    0.0.1-alpha.0.4
 */

?>

<div
    class="container-fluid height-100 background"
    data-ng-init="pageClass = 'business-diversity'; animateClass = 'slideInUp';"
    style="background-image: url('{{backgroundImage}}')"
>
    <div class="background-parent height-100">
        <div class="row">
            <div class="col-xs-12">
                <div class="row">
                    <div class="m-container">
                        <div class="col-sm-6 slideInRight animated">
                            <div class="page-title-category">
                                <h2>{{facilitie.title}}</h2>
                                <a href="{{facilitie.project.back_link}}"><?= Yii::t('content', '< Back to') ?> {{facilitie.project.title}}</a>
                            </div>
                        </div>
                        <div class="col-sm-6 slideInUp animated">
                            <div class="content-scroll-small">
                                <div class="background-scroll">
                                    <div class="scroll" data-ng-scrollbar rebuild-on="rebuild:me">
                                        <div compile="content"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
