<?php
/**
 * File name: plugin.php
 */

return [
    'name' => 'contacs-address',
    'title' => Yii::t('text', 'Contacs Address'),
    'preview_image' => Yii::getAlias('@web').'/application/templates/frontend/base/modules/text/layouts/contacs-address/preview.png',
    'viewFile' => '@app/templates/frontend/base/modules/text/layouts/contacs-address/view.php',
    'settings' => [
        'textHeading' => [
            'type' => 'textarea',
            'value' => 'Even if there is something you have always wanted to experience in interactive visualise solutions and can\'t find it on prof.estate, let us know and we\'ll do our best to find it for you.'
        ],
        'item1' => [
            'type' => 'checkbox',
            'value' => ''
        ],
        'item1PhoneEnable' => [
            'type' => 'checkbox',
            'value' => ''
        ],
        'item1head' => [
            'type' => 'textInput',
            'value' => 'LONDON'
        ],
        'item1PhoneEnable' => [
            'type' => 'checkbox',
            'value' => ''
        ],
        'item1Phone' => [
            'type' => 'textInput',
            'value' => '+44 0 20 3807 1497'
        ],
        'item1MailEnable' => [
            'type' => 'checkbox',
            'value' => ''
        ],
        'item1Mail' => [
            'type' => 'textInput',
            'value' => 'info@prof.estate'
        ],
        'item1Address1lEnable' => [
            'type' => 'checkbox',
            'value' => ''
        ],
        'item1Address1' => [
            'type' => 'textInput',
            'value' => '13 John Prince\'s Street, 2nd Floor,'
        ],
        'item1Address2Enable' => [
            'type' => 'checkbox',
            'value' => ''
        ],
        'item1Address2' => [
            'type' => 'textInput',
            'value' => ''
        ],
        'item1CityCountry' => [
            'type' => 'checkbox',
            'value' => ''
        ],
        'item1CityCountry' => [
            'type' => 'textInput',
            'value' => 'London, England, UK'
        ],
        'item2' => [
            'type' => 'checkbox',
            'value' => ''
        ],
        'item2PhoneEnable' => [
            'type' => 'checkbox',
            'value' => ''
        ],
        'item2head' => [
            'type' => 'textInput',
            'value' => 'KIEV'
        ],
        'item2PhoneEnable' => [
            'type' => 'checkbox',
            'value' => ''
        ],
        'item2Phone' => [
            'type' => 'textInput',
            'value' => '+380 44 222 52 06'
        ],
        'item2MailEnable' => [
            'type' => 'checkbox',
            'value' => ''
        ],
        'item2Mail' => [
            'type' => 'textInput',
            'value' => 'info@prof.estate'
        ],
        'item2Address1lEnable' => [
            'type' => 'checkbox',
            'value' => ''
        ],
        'item2Address1' => [
            'type' => 'textInput',
            'value' => 'BC "Bulgakov"'
        ],
        'item2Address2Enable' => [
            'type' => 'checkbox',
            'value' => ''
        ],
        'item2Address2' => [
            'type' => 'textInput',
            'value' => '17/8 Nyzhniy Val str.'
        ],
        'item2CityCountry' => [
            'type' => 'checkbox',
            'value' => ''
        ],
        'item2CityCountry' => [
            'type' => 'textInput',
            'value' => 'Kiev, Ukraine'
        ],
        'item3' => [
            'type' => 'checkbox',
            'value' => ''
        ],
        'item3PhoneEnable' => [
            'type' => 'checkbox',
            'value' => ''
        ],
        'item3head' => [
            'type' => 'textInput',
            'value' => 'ALMATY'
        ],
        'item3PhoneEnable' => [
            'type' => 'checkbox',
            'value' => ''
        ],
        'item3Phone' => [
            'type' => 'textInput',
            'value' => '+7 727 237 78 08'
        ],
        'item3MailEnable' => [
            'type' => 'checkbox',
            'value' => ''
        ],
        'item3Mail' => [
            'type' => 'textInput',
            'value' => 'info@prof.estate'
        ],
        'item3Address1lEnable' => [
            'type' => 'checkbox',
            'value' => ''
        ],
        'item3Address1' => [
            'type' => 'textInput',
            'value' => 'Bekturova 77A'
        ],
        'item3Address2Enable' => [
            'type' => 'checkbox',
            'value' => ''
        ],
        'item3Address2' => [
            'type' => 'textInput',
            'value' => 'St. Samal-2'
        ],
        'item3CityCountry' => [
            'type' => 'checkbox',
            'value' => ''
        ],
        'item3CityCountry' => [
            'type' => 'textInput',
            'value' => 'Almaty, Kazakhstan'
        ],
        'item4' => [
            'type' => 'checkbox',
            'value' => ''
        ],
        'item4PhoneEnable' => [
            'type' => 'checkbox',
            'value' => ''
        ],
        'item4head' => [
            'type' => 'textInput',
            'value' => 'HO CHI MINH CITY'
        ],
        'item4PhoneEnable' => [
            'type' => 'checkbox',
            'value' => ''
        ],
        'item4Phone' => [
            'type' => 'textInput',
            'value' => '+7 499 322 84 91'
        ],
        'item4MailEnable' => [
            'type' => 'checkbox',
            'value' => ''
        ],
        'item4Mail' => [
            'type' => 'textInput',
            'value' => ''
        ],
        'item4Address1lEnable' => [
            'type' => 'checkbox',
            'value' => ''
        ],
        'item4Address1' => [
            'type' => 'textInput',
            'value' => '38, floor 3, 26 Ly Tu Trong street,'
        ],
        'item4Address2Enable' => [
            'type' => 'checkbox',
            'value' => ''
        ],
        'item4Address2' => [
            'type' => 'textInput',
            'value' => 'Ben Nghe ward, district 1, HCMC'
        ],
        'item4CityCountry' => [
            'type' => 'checkbox',
            'value' => ''
        ],
        'item4CityCountry' => [
            'type' => 'textInput',
            'value' => 'Ho Chi Minh City, Vietnam'
        ],
    ],
];