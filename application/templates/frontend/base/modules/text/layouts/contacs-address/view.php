<?php
/**
 * File name: view.php
 *
 * @var $model \app\modules\text\models\Text;
 */

use yii\helpers\Url;

$isHome = (Yii::$app->request->baseUrl.'/index' == Url::to([''])) ? true : false;
?>

<div class="contact-second-block clearfix">
    <div class="content csb-content">
        <div class="c-text font-size">
            <?= $model->settings['textHeading']['value'] ?>
        </div>
    </div>
    <div class="csb-wrapper clearfix">
        <?php if($model->getSetting('item1', 0)):?>
            <div class="csb-list-wrapper">
                <h4 class="contact-h4">
                    <?= $model->settings['item1head']['value'] ?>
                </h4>
                <ul class="csb-cnt">
                    <?php if($model->getSetting('item1PhoneEnable', 0)):?>
                        <li class="csb-item">
                            <a href="tel:<?= $model->settings['item1Phone']['value'] ?>">
                                <?= $model->settings['item1Phone']['value'] ?>
                            </a>
                        </li>
                    <?php endif; ?>

                    <?php if($model->getSetting('item1MailEnable', 0)):?>
                        <li class="csb-item">
                            <a href="mailto:<?= $model->settings['item1Mail']['value'] ?>">
                                <?= $model->settings['item1Mail']['value'] ?>
                            </a>
                        </li>
                    <?php endif; ?>

                    <?php if($model->getSetting('item1Address1lEnable', 0)):?>
                        <li class="csb-item">
                            <?= $model->settings['item1Address1']['value'] ?>
                        </li>
                    <?php endif; ?>

                    <?php if($model->getSetting('item1Address2Enable', 0)):?>
                        <li class="csb-item">
                            <?= $model->settings['item1Address2']['value'] ?>
                        </li>
                    <?php endif; ?>

                    <?php if($model->getSetting('item1CityCountry', 0)):?>
                        <li class="csb-item">
                            <?= $model->settings['item1CityCountry']['value'] ?>
                        </li>
                    <?php endif; ?>
                </ul>
            </div>
        <?php endif; ?>

        <?php if($model->getSetting('item2', 0)):?>
            <div class="csb-list-wrapper">
                <h4 class="contact-h4">
                    <?= $model->settings['item2head']['value'] ?>
                </h4>
                <ul class="csb-cnt">
                    <?php if($model->getSetting('item2PhoneEnable', 0)):?>
                        <li class="csb-item">
                            <a href="tel:<?= $model->settings['item2Phone']['value'] ?>">
                                <?= $model->settings['item2Phone']['value'] ?>
                            </a>
                        </li>
                    <?php endif; ?>

                    <?php if($model->getSetting('item2MailEnable', 0)):?>
                        <li class="csb-item">
                            <a href="mailto:<?= $model->settings['item2Mail']['value'] ?>">
                                <?= $model->settings['item2Mail']['value'] ?>
                            </a>
                        </li>
                    <?php endif; ?>

                    <?php if($model->getSetting('item2Address1lEnable', 0)):?>
                        <li class="csb-item">
                            <?= $model->settings['item2Address1']['value'] ?>
                        </li>
                    <?php endif; ?>

                    <?php if($model->getSetting('item2Address2Enable', 0)):?>
                        <li class="csb-item">
                            <?= $model->settings['item2Address2']['value'] ?>
                        </li>
                    <?php endif; ?>

                    <?php if($model->getSetting('item2CityCountry', 0)):?>
                        <li class="csb-item">
                            <?= $model->settings['item2CityCountry']['value'] ?>
                        </li>
                    <?php endif; ?>
                </ul>
            </div>
        <?php endif; ?>

        <?php if($model->getSetting('item3', 0)):?>
            <div class="csb-list-wrapper">
                <h4 class="contact-h4">
                    <?= $model->settings['item3head']['value'] ?>
                </h4>
                <ul class="csb-cnt">
                    <?php if($model->getSetting('item3PhoneEnable', 0)):?>
                        <li class="csb-item">
                            <a href="tel:<?= $model->settings['item3Phone']['value'] ?>">
                                <?= $model->settings['item3Phone']['value'] ?>
                            </a>
                        </li>
                    <?php endif; ?>

                    <?php if($model->getSetting('item3MailEnable', 0)):?>
                        <li class="csb-item">
                            <a href="mailto:<?= $model->settings['item3Mail']['value'] ?>">
                                <?= $model->settings['item3Mail']['value'] ?>
                            </a>
                        </li>
                    <?php endif; ?>

                    <?php if($model->getSetting('item3Address1lEnable', 0)):?>
                        <li class="csb-item">
                            <?= $model->settings['item3Address1']['value'] ?>
                        </li>
                    <?php endif; ?>

                    <?php if($model->getSetting('item3Address2Enable', 0)):?>
                        <li class="csb-item">
                            <?= $model->settings['item3Address2']['value'] ?>
                        </li>
                    <?php endif; ?>

                    <?php if($model->getSetting('item3CityCountry', 0)):?>
                        <li class="csb-item">
                            <?= $model->settings['item3CityCountry']['value'] ?>
                        </li>
                    <?php endif; ?>
                </ul>
            </div>
        <?php endif; ?>

        <?php if($model->getSetting('item4', 0)):?>
            <div class="csb-list-wrapper">
                <h4 class="contact-h4">
                    <?= $model->settings['item4head']['value'] ?>
                </h4>
                <ul class="csb-cnt">
                    <?php if($model->getSetting('item4PhoneEnable', 0)):?>
                        <li class="csb-item">
                            <a href="tel:<?= $model->settings['item3Phone']['value'] ?>">
                                <?= $model->settings['item4Phone']['value'] ?>
                            </a>
                        </li>
                    <?php endif; ?>

                    <?php if($model->getSetting('item4MailEnable', 0)):?>
                        <li class="csb-item">
                            <a href="mailto:<?= $model->settings['item3Mail']['value'] ?>">
                                <?= $model->settings['item4Mail']['value'] ?>
                            </a>
                        </li>
                    <?php endif; ?>

                    <?php if($model->getSetting('item4Address1lEnable', 0)):?>
                        <li class="csb-item">
                            <?= $model->settings['item4Address1']['value'] ?>
                        </li>
                    <?php endif; ?>

                    <?php if($model->getSetting('item4Address2Enable', 0)):?>
                        <li class="csb-item">
                            <?= $model->settings['item4Address2']['value'] ?>
                        </li>
                    <?php endif; ?>

                    <?php if($model->getSetting('item4CityCountry', 0)):?>
                        <li class="csb-item">
                            <?= $model->settings['item4CityCountry']['value'] ?>
                        </li>
                    <?php endif; ?>
                </ul>
            </div>
        <?php endif; ?>
    </div>
</div>