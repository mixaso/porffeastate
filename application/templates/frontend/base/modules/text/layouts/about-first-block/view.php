<?php
/**
 * File name: view.php
 *
 * @var $model \app\modules\text\models\Text;
 */

use yii\helpers\Url;

$isHome = (Yii::$app->request->baseUrl.'/index' == Url::to([''])) ? true : false;
?>

<div class="about-first-block clearfix">
    <div class="aside-left">
        <h1><?= $model->settings['heading']['value'] ?></h1>
    </div>
    <div class="content afb-content">
        <div class="c-text afb-text font-size-smaller">
            <?= $model->settings['text']['value'] ?>
        </div>
    </div>
</div>