<?php
/**
 * File name: plugin.php
 */

return [
    'name' => 'about-first-block',
    'title' => Yii::t('text', 'About First Block'),
    'preview_image' => Yii::getAlias('@web').'/application/templates/frontend/base/modules/text/layouts/about-first-block/preview.png',
    'viewFile' => '@app/templates/frontend/base/modules/text/layouts/about-first-block/view.php',
    'settings' => [
        'heading' => [
            'type' => 'textInput',
            'value' => 'Our team'
        ],
        'text' => [
            'type' => 'textarea',
            'value' => 'Every business case for us is a great opportunity not only to meet the KPIs,<br>
            but also to use our global know-how in practice.'
        ]
    ],
];
