<?php
/**
 * File name: plugin.php
 */

return [
    'name' => 'about-fifth-block',
    'title' => Yii::t('text', 'About Fifth Block'),
    'preview_image' => Yii::getAlias('@web').'/application/templates/frontend/base/modules/text/layouts/about-fifth-block/preview.png',
    'viewFile' => '@app/templates/frontend/base/modules/text/layouts/about-fifth-block/view.php',
    'settings' => [
        'leftImage' => [
            'type' => 'mediaInput',
            'value' => 'images/offce2.jpg'
        ],
        'leftImageAlt' => [
            'type' => 'textInput',
            'value' => ''
        ],
        'rightImage' => [
            'type' => 'mediaInput',
            'value' => 'images/content-pc-about-2x.jpg'
        ],
        'rightImageAlt' => [
            'type' => 'textInput',
            'value' => ''
        ],
    ],
];
