<?php
/**
 * File name: view.php
 *
 * @var $model \app\modules\text\models\Text;
 */

use yii\helpers\Url;

$isHome = (Yii::$app->request->baseUrl.'/index' == Url::to([''])) ? true : false;
?>

<div class="about-fifth-block clearfix">
    <div class="afb-image-wrapper">
        <img src="<?= $model->settings['leftImage']['value'] ?>" alt="<?= $model->settings['leftImageAlt']['value'] ?>">
    </div>
    <div class="afb-image-wrapper">
        <img src="<?= $model->settings['rightImage']['value'] ?>" alt="<?= $model->settings['rightImageAlt']['value']
        ?>">
    </div>
</div>