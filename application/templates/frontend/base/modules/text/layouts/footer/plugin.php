<?php
/**
 * File name: plugin.php
 */

return [
    'name' => 'footer',
    'title' => Yii::t('text', 'Footer'),
    'preview_image' => Yii::getAlias('@web').'/application/templates/frontend/base/modules/text/layouts/footer/preview.png',
    'viewFile' => '@app/templates/frontend/base/modules/text/layouts/footer/view.php',
    'settings' => [
        'logo' => [
            'type'=> 'mediaInput',
            'value' => 'images/logo-white.png'
        ],
        'logoAlt' => [
            'type'=> 'textInput',
            'value' => ''
        ],
        'menu' => [
            'type'=> 'menuType',
            'value' => ''
        ],
        'socialHeading' => [
            'type' => 'textInput',
            'value' => 'Follow us'
        ],
        'social1' => [
            'type' => 'checkbox',
            'value' => ''
        ],
        'linkSocial1' => [
            'type' => 'textInput',
            'value' => '//facebook.com'
        ],
        'textSocial1' => [
            'type' => 'textInput',
            'value' => 'Facebook'
        ],
        'social2' => [
            'type' => 'checkbox',
            'value' => ''
        ],
        'linkSocial2' => [
            'type' => 'textInput',
            'value' => '//twitter.com'
        ],
        'textSocial2' => [
            'type' => 'textInput',
            'value' => 'Twitter'
        ],
        'social3' => [
            'type' => 'checkbox',
            'value' => ''
        ],
        'linkSocial3' => [
            'type' => 'textInput',
            'value' => '//linkedin.com'
        ],
        'textSocial3' => [
            'type' => 'textInput',
            'value' => 'Linkedin'
        ],
        'social4' => [
            'type' => 'checkbox',
            'value' => ''
        ],
        'linkSocial4' => [
            'type' => 'textInput',
            'value' => '#'
        ],
        'textSocial4' => [
            'type' => 'textInput',
            'value' => ''
        ],
        'social5' => [
            'type' => 'checkbox',
            'value' => ''
        ],
        'linkSocial5' => [
            'type' => 'textInput',
            'value' => '#'
        ],
        'textSocial5' => [
            'type' => 'textInput',
            'value' => ''
        ],
        'social6' => [
            'type' => 'checkbox',
            'value' => ''
        ],
        'linkSocial6' => [
            'type' => 'textInput',
            'value' => '#'
        ],
        'textSocial6' => [
            'type' => 'textInput',
            'value' => ''
        ],
        'officephone' => [
            'type' => 'textInput',
            'value' => '+380 44 222 52 06'
        ],
        'officemail' => [
            'type' => 'textInput',
            'value' => 'info@prof.estate'
        ],
    ],
];
