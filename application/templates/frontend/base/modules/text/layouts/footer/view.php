<?php
/**
 * File name: view.php
 *
 * @var $model \app\modules\text\models\Text;
 */

use yii\helpers\Url;
use app\modules\menu\api\Menu as ApiMenu;

$isHome = (Yii::$app->request->baseUrl.'/index' == Url::to([''])) ? true : false;
?>

<footer>
    <ul class="footer-content clearfix">
        <li class="f-column">
            <h4 class="f-column-title">Contacts</h4>
            <div class="f-address">
                <?= $model->settings['officephone']['value'] ?><br />
                <a href="mailto:<?= $model->settings['officemail']['value'] ?>"><?= $model->settings['officemail']['value'] ?></a><br />
            </div>
        </li>
        <li class="f-column">
            <h4 class="f-column-title"><?= $model->settings['socialHeading']['value'] ?></h4>
            <div class="f-social-links">
                <?php if($model->getSetting('social1', 0)):?>
                    <a href="<?= $model->settings['linkSocial1']['value'] ?>" target="_blank"><?= $model->settings['textSocial1']['value'] ?></a><br />
                <?php endif; ?>

                <?php if($model->getSetting('social2', 0)):?>
                    <a href="<?= $model->settings['linkSocial2']['value'] ?>" target="_blank"><?= $model->settings['textSocial2']['value'] ?></a><br />
                <?php endif; ?>

                <?php if($model->getSetting('social3', 0)):?>
                    <a href="<?= $model->settings['linkSocial3']['value'] ?>" target="_blank"><?= $model->settings['textSocial3']['value'] ?></a><br />
                <?php endif; ?>

                <?php if($model->getSetting('social4', 0)):?>
                    <a href="<?= $model->settings['linkSocial4']['value'] ?>" target="_blank"><?= $model->settings['textSocial4']['value'] ?></a><br />
                <?php endif; ?>

                <?php if($model->getSetting('social5', 0)):?>
                    <a href="<?= $model->settings['linkSocial5']['value'] ?>" target="_blank"><?= $model->settings['textSocial5']['value'] ?></a><br />
                <?php endif; ?>

                <?php if($model->getSetting('social6', 0)):?>
                    <a href="<?= $model->settings['linkSocial6']['value'] ?>" target="_blank"><?= $model->settings['textSocial6']['value'] ?></a><br />
                <?php endif; ?>
            </div>
        </li>
        <li class="footer-logo-wrapper">
            <a class="footer-logo" href="/">
                <img alt="<?= $model->settings['logoAlt']['value'] ?>" src="<?= $model->settings['logo']['value']
                ?>">
            </a>
        </li>
        <li class="f-privacy-wrapper">
            <div class="f-privacy-links">
                Copyright@Prof.Estate
            </div>
        </li>
        <li class="f-column">
            <?php
            $items = ApiMenu::getMenuLvl($model->getSetting('menu'), 1, 2);
            echo \yii\bootstrap\Nav::widget([
                'items' => $items,
                'options' => [
                    'class' => 'footer__nav'
                ]
            ]);
            //            echo Menu::widget()
            ?>
        </li>
    </ul>
</footer>
<div id="video-popup" class="video-popup">
    <div class="page-popup-cnt">
        <video class="video" poster="/video/example/example2.jpg" preload="auto" muted="muted" controls="controls">
            <source type="video/mp4" src="/video/example/example2.mp4"/>
            <p>Your user agent does not support the HTML5 Video element.</p>
        </video>
    </div>
</div>
<a class="video-popup-link none" href="#" popupid="video-popup" style="cursor: pointer;">Call us!</a>