<?php
/**
 * File name: view.php
 *
 * @var $model \app\modules\text\models\Text;
 */

use yii\helpers\Url;

$isHome = (Yii::$app->request->baseUrl.'/index' == Url::to([''])) ? true : false;
?>

<div class="contacts-map-block clearfix">
    <div class="aside-left">
        <h1>
            <?= $model->settings['heading']['value'] ?>
        </h1>
    </div>
    <div class="content cmb-content clearfix">
        <div class="c-text font-size-smaller"><?= $model->settings['text']['value'] ?></div>
        <div class="map-wrapper">
            <div class="cmd-map" id="map"></div>
        </div>
    </div>
</div>