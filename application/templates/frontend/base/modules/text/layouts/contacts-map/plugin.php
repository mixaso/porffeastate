<?php
/**
 * File name: plugin.php
 */

return [
    'name' => 'contacts-map',
    'title' => Yii::t('text', 'Contacts Map'),
    'preview_image' => Yii::getAlias('@web').'/application/templates/frontend/base/modules/text/layouts/contacts-map/preview.png',
    'viewFile' => '@app/templates/frontend/base/modules/text/layouts/contacts-map/view.php',
    'settings' => [
        'heading' => [
            'type' => 'textInput',
            'value' => 'Hello World!'
        ],
        'text' => [
            'type' => 'textarea',
            'value' => 'We are here to answer any questions you may have about our prof.estate experience. Reach out to us and we\'ll response as soon as we can.'
        ]
    ],
];
