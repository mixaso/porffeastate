<?php
/**
 * File name: view.php
 *
 * @var $model \app\modules\text\models\Text;
 */

use yii\helpers\Url;
use app\modules\menu\api\Menu as ApiMenu;

$isHome = (Yii::$app->request->baseUrl.'/index' == Url::to([''])) ? true : false;
?>

<div class="first-block clearfix">
    <div class="aside-left">
    </div>
    <div class="content fb-content">
        <div class="c-text fb-text font-size">
            <?= $model->settings['text']['value'] ?>
        </div>
    </div>
</div>