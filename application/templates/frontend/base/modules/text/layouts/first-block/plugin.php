<?php
/**
 * File name: plugin.php
 */

return [
    'name' => 'first-block',
    'title' => Yii::t('text', 'First Block'),
    'preview_image' => Yii::getAlias('@web').'/application/templates/frontend/base/modules/text/layouts/first-block/preview.png',
    'viewFile' => '@app/templates/frontend/base/modules/text/layouts/first-block/view.php',
    'settings' => [
        'text' => [
            'type' => 'textarea',
            'value' => ' When selling off-plan properties print materials, static renders, booklets are not efficient anymore. Customers want to engage with the project, explore it, find out what is it they are actually buying. Provide us with the architectural plans of your project and we will develop a software, which will allow property buyers to fully explore the project.<br><br>
            They will be able to look at it from any side, explore the surroundings, location, infrastructure, browse through all pictures, videos etc. If they like the project they can look at all floorplans, walk into a virtual model of the apartments they are considering, get a feel for their size, layout and even the view they\'ll have out of the windows. All that - to create an emotional bond with the project and drive sales results!'
        ]
    ],
];
