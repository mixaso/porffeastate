<?php
/**
 * Created by Vladimir Hryvinskyy.
 * Site: http://codice.in.ua/
 * Date: 16.09.2016
 * Project: osnovasite
 * File name: view.php
 *
 * @var $model \app\modules\text\models\Text;
 */

use yii\helpers\Url;
use yii\helpers\Html;


$isHome = (Yii::$app->request->baseUrl.'/index' == Url::to([''])) ? true : false;
?>


<?php if($model->getSetting('enableFrame', 0)):?>
<div class="modal fade" id="<?= $model->getSetting('id') ?>" tabindex="-1" role="dialog" aria-labelledby="<?= $model->getSetting('id') ?>">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><img src="/uploads/close.svg" data-uk-svg></button>
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-body">
                    <?= $model->getSetting('frame') ?>
            </div>
        </div>
    </div>
</div>
<?php endif; ?>

<?php if($model->getSetting('enableWidget', 0)):?>
    <div id="<?= $model->getSetting('id') ?>" class="uk-modal">
        <div class="uk-modal-dialog">
            <a class="uk-modal-close uk-close"></a>
            [widgetkit id='<?= $model->getSetting('widget') ?>']
        </div>
    </div>
<?php endif; ?>