<?php
/**
 * Created by Vladimir Hryvinskyy.
 * Site: http://codice.in.ua/
 * Date: 16.09.2016
 * Project: osnovasite
 * File name: plugin.php
 */

return [
    'name' => 'modal',
    'title' => Yii::t('text', 'Modal'),
    'preview_image' => Yii::getAlias('@web').'/application/templates/frontend/base/modules/text/layouts/modal/preview.png',
    'viewFile' => '@app/templates/frontend/base/modules/text/layouts/modal/view.php',
    'settings' => [
        'id' => [
            'type' => 'textInput',
            'value' => ''
        ],
        'enableFrame' => [
            'type' => 'checkbox',
            'value' => ''
        ],
        'frame' => [
            'type' => 'textInput',
            'value' => ''
        ],
        'enableWidget' => [
            'type' => 'checkbox',
            'value' => ''
        ],
        'widget' => [
            'type' => 'widgetkit',
            'value' => ''
        ]
    ],
];
