<?php
/**
 * File name: view.php
 *
 * @var $model \app\modules\text\models\Text;
 */

use yii\helpers\Url;

$isHome = (Yii::$app->request->baseUrl.'/index' == Url::to([''])) ? true : false;
?>

<div class="productTabs">
    <div class="wbb-cnt">
        <h3><?= $model->settings['heading']['value'] ?></h3>
        <!-- Nav tabs -->
        <ul class="nav nav-tabs" role="tablist">
            <li role="presentation" class="active">
                <a href="<?php echo Url::to() ?>#<?= $model->settings['productTabName1']['value'] ?>" role="tab"
                   data-toggle="tab" data-target="#<?= $model->settings['productTabName1']['value'] ?>">
                    <?= $model->settings['productTabTitle1']['value'] ?>
                </a>

            </li>
            <li role="presentation">
                <a href="<?php echo Url::to() ?>#<?= $model->settings['productTabName2']['value'] ?>" role="tab"
                   data-toggle="tab" data-target="#<?= $model->settings['productTabName2']['value'] ?>">
                    <?= $model->settings['productTabTitle2']['value'] ?>
                </a>
            </li>
            <li role="presentation">
                <a href="<?php echo Url::to() ?>#<?= $model->settings['productTabName3']['value'] ?>" role="tab"
                   data-toggle="tab" data-target="#<?= $model->settings['productTabName3']['value'] ?>">
                    <?= $model->settings['productTabTitle3']['value'] ?>
                </a>
            </li>
            <li role="presentation">
                <a href="<?php echo Url::to() ?>#<?= $model->settings['productTabName4']['value'] ?>" role="tab"
                   data-toggle="tab" data-target="#<?= $model->settings['productTabName4']['value'] ?>">
                    <?= $model->settings['productTabTitle4']['value'] ?>
                </a>
            </li>
            <li role="presentation">
                <a href="<?php echo Url::to() ?>#<?= $model->settings['productTabName5']['value'] ?>" role="tab" data-toggle="tab" data-target="#<?= $model->settings['productTabName5']['value'] ?>">
                    <?= $model->settings['productTabTitle5']['value'] ?>
                </a>
            </li>
        </ul>

        <!-- Tab panes -->
        <div class="tab-content productTabs__content">
            <div role="tabpanel" class="row tab-pane fade in active" id="<?= $model->settings['productTabName1']['value']
            ?>">
                <div class="productTabs__content_media">
                    <img src="<?= $model->settings['productTabMedia1']['value'] ?>" alt="<?= $model->settings['productTabMedia1Alt']['value'] ?>">
                </div>
                <div class="productTabs__content_text">
                    <div class="productTabs__content_text_in">
                        <?= $model->settings['productTabText1']['value'] ?>
                    </div>
                </div>
            </div>
            <div role="tabpanel" class="row tab-pane fade" id="<?= $model->settings['productTabName2']['value'] ?>">
                <div class="productTabs__content_media">
                    <img src="<?= $model->settings['productTabMedia2']['value'] ?>" alt="<?= $model->settings['productTabMedia2Alt']['value'] ?>">
                </div>
                <div class="productTabs__content_text">
                    <div class="productTabs__content_text_in">
                        <?= $model->settings['productTabText2']['value'] ?>
                    </div>
                </div>
            </div>
            <div role="tabpanel" class="row tab-pane fade" id="<?= $model->settings['productTabName3']['value'] ?>">
                <div class="productTabs__content_media">
                    <img src="<?= $model->settings['productTabMedia3']['value'] ?>" alt="<?= $model->settings['productTabMedia3Alt']['value'] ?>">
                </div>
                <div class="productTabs__content_text">
                    <div class="productTabs__content_text_in">
                        <?= $model->settings['productTabText3']['value'] ?>
                    </div>
                </div>
            </div>
            <div role="tabpanel" class="row tab-pane fade" id="<?= $model->settings['productTabName4']['value'] ?>">
                <div class="productTabs__content_media">
                    <img src="<?= $model->settings['productTabMedia4']['value'] ?>" alt="<?= $model->settings['productTabMedia4Alt']['value'] ?>">
                </div>
                <div class="productTabs__content_text">
                    <div class="productTabs__content_text_in">
                        <?= $model->settings['productTabText4']['value'] ?>
                    </div>
                </div>
            </div>
            <div role="tabpanel" class="row tab-pane fade" id="<?= $model->settings['productTabName5']['value'] ?>">
                <div class="productTabs__content_media">
                    <img src="<?= $model->settings['productTabMedia5']['value'] ?>" alt="<?= $model->settings['productTabMedia5Alt']['value'] ?>">
                </div>
                <div class="productTabs__content_text">
                    <div class="productTabs__content_text_in">
                        <?= $model->settings['productTabText5']['value'] ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>