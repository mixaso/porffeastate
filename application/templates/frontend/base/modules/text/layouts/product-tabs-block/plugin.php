<?php
/**
 * File name: plugin.php
 */

return [
    'name' => 'product-tabs-block',
    'title' => Yii::t('text', 'Product Tabs Block'),
    'preview_image' => Yii::getAlias('@web').'/application/templates/frontend/base/modules/text/layouts/product-tabs-block/preview.png',
    'viewFile' => '@app/templates/frontend/base/modules/text/layouts/product-tabs-block/view.php',
    'settings' => [
        'heading' => [
            'type' => 'textInput',
            'value' => 'WHY PROF.ESTATE'
        ],
        'productTabName1' => [
            'type' => 'textInput',
            'value' => ''
        ],
        'productTabTitle1' => [
            'type' => 'textInput',
            'value' => ''
        ],
        'productTabText1' => [
            'type' => 'textarea',
            'value' => ''
        ],
        'productTabMedia1' => [
            'type' => 'mediaInput',
            'value' => ''
        ],
        'productTabMedia1Alt' => [
            'type' => 'textInput',
            'value' => ''
        ],
        'productTabName2' => [
            'type' => 'textInput',
            'value' => ''
        ],
        'productTabTitle2' => [
            'type' => 'textInput',
            'value' => ''
        ],
        'productTabText2' => [
            'type' => 'textarea',
            'value' => ''
        ],
        'productTabMedia2' => [
            'type' => 'mediaInput',
            'value' => ''
        ],
        'productTabMedia2Alt' => [
            'type' => 'textInput',
            'value' => ''
        ],
        'productTabName3' => [
            'type' => 'textInput',
            'value' => ''
        ],
        'productTabTitle3' => [
            'type' => 'textInput',
            'value' => ''
        ],
        'productTabText3' => [
            'type' => 'textarea',
            'value' => ''
        ],
        'productTabMedia3' => [
            'type' => 'mediaInput',
            'value' => ''
        ],
        'productTabMedia3Alt' => [
            'type' => 'textInput',
            'value' => ''
        ],
        'productTabName4' => [
            'type' => 'textInput',
            'value' => ''
        ],
        'productTabTitle4' => [
            'type' => 'textInput',
            'value' => ''
        ],
        'productTabText4' => [
            'type' => 'textarea',
            'value' => ''
        ],
        'productTabMedia4' => [
            'type' => 'mediaInput',
            'value' => ''
        ],
        'productTabMedia4Alt' => [
            'type' => 'textInput',
            'value' => ''
        ],
        'productTabName5' => [
            'type' => 'textInput',
            'value' => ''
        ],
        'productTabTitle5' => [
            'type' => 'textInput',
            'value' => ''
        ],
        'productTabText5' => [
            'type' => 'textarea',
            'value' => ''
        ],
        'productTabMedia5' => [
            'type' => 'mediaInput',
            'value' => ''
        ],
        'productTabMedia5Alt' => [
            'type' => 'textInput',
            'value' => ''
        ],
    ],
];