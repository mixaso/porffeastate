<?php
/**
 * File name: view.php
 *
 * @var $model \app\modules\text\models\Text;
 */

use yii\helpers\Url;

$isHome = (Yii::$app->request->baseUrl.'/index' == Url::to([''])) ? true : false;
?>

<div class="work-pride-block clearfix">
    <aside class="aside-left">
        <h1><?= $model->settings['heading']['value'] ?></h1>
    </aside>
    <div class="wpb-content clearfix">
        <div class="c-text font-size-smaller"><?= $model->settings['text']['value'] ?></div>
    </div>
</div>