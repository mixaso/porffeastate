<?php
/**
 * File name: plugin.php
 */

return [
    'name' => 'product-first-block',
    'title' => Yii::t('text', 'Product First Block'),
    'preview_image' => Yii::getAlias('@web').'/application/templates/frontend/base/modules/text/layouts/product-first-block/preview.png',
    'viewFile' => '@app/templates/frontend/base/modules/text/layouts/product-first-block/view.php',
    'settings' => [
        'heading' => [
            'type' => 'textInput',
            'value' => 'Our pride'
        ],
        'text' => [
            'type' => 'textarea',
            'value' => 'We are honored to present touchscreen panel - interactive solution, which allows to visualise real estate projects of any scale in an engaging manner for real estate buyers, using various types of touchscreens complexity.'
        ]
    ],
];