<?php
/**
 * File name: view.php
 *
 * @var $model \app\modules\text\models\Text;
 */

use yii\helpers\Url;
use app\modules\menu\api\Menu as ApiMenu;

$isHome = (Yii::$app->request->baseUrl.'/index' == Url::to([''])) ? true : false;
?>

<div class="works-block clearfix">
    <div class="wb-aside-left">
        <h2 class="wb-h2"><?= $model->settings['heading']['value'] ?></h2>
    </div>
    <?php if($model->getSetting('activateBlock1', 0)):?>
        <div class="square-box <?= $model->settings['widthBlock1']['value'] ?>">
            <div class="square-content">
                <video class="video portrait" poster="<?= $model->settings['posterBlock1']['value'] ?>" preload="auto" muted="muted" loop="">
                    <source type="video/mp4" src="<?= $model->settings['videoBlock1']['value'] ?>">
                    <p>Your user agent does not support the HTML5 Video element.</p>
                </video>
                <div class="blue-gradient"></div>
                <div class="square-content-text">
                    <a href="<?= $model->settings['linkBlock1']['value'] ?>">
                        <span><?= $model->settings['textBlock1']['value'] ?></span>
                    </a>
                </div>
            </div>
        </div>
    <?php endif; ?>

    <?php if($model->getSetting('activateBlock2', 0)):?>
        <div class="square-box <?= $model->settings['widthBlock2']['value'] ?>">
            <div class="square-content">
                <video class="video portrait" poster="<?= $model->settings['posterBlock2']['value'] ?>"
                       preload="auto" muted="muted" loop="">
                    <source type="video/mp4" src="<?= $model->settings['videoBlock2']['value'] ?>">
                    <p>Your user agent does not support the HTML5 Video element.</p>
                </video>
                <div class="blue-gradient"></div>
                <div class="square-content-text">
                    <a href="<?= $model->settings['linkBlock2']['value'] ?>">
                        <span><?= $model->settings['textBlock2']['value'] ?></span>
                    </a>
                </div>
            </div>
        </div>
    <?php endif; ?>

    <?php if($model->getSetting('activateBlock3', 0)):?>
        <div class="square-box <?= $model->settings['widthBlock3']['value'] ?>">
            <div class="square-content">
                <video class="video portrait" poster="<?= $model->settings['posterBlock3']['value'] ?>"
                       preload="auto" muted="muted" loop="">
                    <source type="video/mp4" src="<?= $model->settings['videoBlock3']['value'] ?>">
                    <p>Your user agent does not support the HTML5 Video element.</p>
                </video>
                <div class="blue-gradient"></div>
                <div class="square-content-text">
                    <a href="<?= $model->settings['linkBlock3']['value'] ?>">
                        <span><?= $model->settings['textBlock3']['value'] ?></span>
                    </a>
                </div>
            </div>
        </div>
    <?php endif; ?>

    <?php if($model->getSetting('activateBlock4', 0)):?>
        <div class="square-box <?= $model->settings['widthBlock4']['value'] ?>">
            <div class="square-content">
                <video class="video portrait" poster="<?= $model->settings['posterBlock4']['value'] ?>"
                       preload="auto" muted="muted" loop="">
                    <source type="video/mp4" src="<?= $model->settings['videoBlock4']['value'] ?>">
                    <p>Your user agent does not support the HTML5 Video element.</p>
                </video>
                <div class="blue-gradient"></div>
                <div class="square-content-text">
                    <a href="<?= $model->settings['linkBlock4']['value'] ?>">
                        <span><?= $model->settings['textBlock4']['value'] ?></span>
                    </a>
                </div>
            </div>
        </div>
    <?php endif; ?>

    <?php if($model->getSetting('activateBlock5', 0)):?>
        <div class="square-box <?= $model->settings['widthBlock5']['value'] ?>">
            <div class="square-content">
                <video class="video portrait" poster="<?= $model->settings['posterBlock5']['value'] ?>"
                       preload="auto" muted="muted" loop="">
                    <source type="video/mp4" src="<?= $model->settings['videoBlock5']['value'] ?>">
                    <p>Your user agent does not support the HTML5 Video element.</p>
                </video>
                <div class="blue-gradient"></div>
                <div class="square-content-text">
                    <a href="<?= $model->settings['linkBlock5']['value'] ?>">
                        <span><?= $model->settings['textBlock5']['value'] ?></span>
                    </a>
                </div>
            </div>
        </div>
    <?php endif; ?>

    <?php if($model->getSetting('activateBlock6', 0)):?>
        <div class="square-box <?= $model->settings['widthBlock6']['value'] ?>">
            <div class="square-content">
                <video class="video portrait" poster="<?= $model->settings['posterBlock6']['value'] ?>"
                       preload="auto" muted="muted" loop="">
                    <source type="video/mp4" src="<?= $model->settings['videoBlock6']['value'] ?>">
                    <p>Your user agent does not support the HTML5 Video element.</p>
                </video>
                <div class="blue-gradient"></div>
                <div class="square-content-text">
                    <a href="<?= $model->settings['linkBlock6']['value'] ?>">
                        <span><?= $model->settings['textBlock6']['value'] ?></span>
                    </a>
                </div>
            </div>
        </div>
    <?php endif; ?>

    <?php if($model->getSetting('activateBlock7', 0)):?>
        <div class="square-box <?= $model->settings['widthBlock7']['value'] ?>">
            <div class="square-content">
                <video class="video portrait" poster="<?= $model->settings['posterBlock7']['value'] ?>"
                       preload="auto" muted="muted" loop="">
                    <source type="video/mp4" src="<?= $model->settings['videoBlock7']['value'] ?>">
                    <p>Your user agent does not support the HTML5 Video element.</p>
                </video>
                <div class="blue-gradient"></div>
                <div class="square-content-text">
                    <a href="<?= $model->settings['linkBlock7']['value'] ?>">
                        <span><?= $model->settings['textBlock7']['value'] ?></span>
                    </a>
                </div>
            </div>
        </div>
    <?php endif; ?>

    <?php if($model->getSetting('activateBlockMoreWorks', 0)):?>
        <div class="square-box <?= $model->settings['widthBlockMoreWorks']['value'] ?>" >
            <div class="blue-gradient"></div>
            <div class='square-content'>
                <div>
                    <a href="<?= $model->settings['linkBlockMoreWorks']['value'] ?>">
                        <span><?= $model->settings['textBlockMoreWorks']['value'] ?></span>
                    </a>
                </div>
            </div>
        </div>
    <?php endif; ?>
</div>