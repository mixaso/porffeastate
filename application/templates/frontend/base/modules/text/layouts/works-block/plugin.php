<?php
/**
 * File name: plugin.php
 */

return [
    'name' => 'works-block',
    'title' => Yii::t('text', 'Works Block'),
    'preview_image' => Yii::getAlias('@web').'/application/templates/frontend/base/modules/text/layouts/works-block/preview.png',
    'viewFile' => '@app/templates/frontend/base/modules/text/layouts/works-block/view.php',
    'settings' => [
        'heading' => [
            'type' => 'textInput',
            'value' => 'Product features'
        ],
        'activateBlock1' => [
            'type' => 'checkbox',
            'value' => ''
        ],
        'widthBlock1' => [
            'type' => 'select',
            'value' => '',
            'items' => [
                'fifty'=>'50%',
                'twenty-five'=>'25%',
                'rectangle'=>'rectangle'
            ]
        ],
        'posterBlock1' => [
            'type' => 'mediaInput',
            'value' => 'video/blocks/1INTERACTIVE-MODEL-OF-PROPERTY.jpg'
        ],
        'videoBlock1' => [
            'type' => 'mediaInput',
            'value' => 'video/blocks/720p/1INTERACTIVE-MODEL-OF-PROPERTY.mp4'
        ],
        'linkBlock1' => [
            'type' => 'textInput',
            'value' => '#'
        ],
        'textBlock1' => [
            'type' => 'textarea',
            'value' => 'Interactive model of property'
        ],
        'activateBlock2' => [
            'type' => 'checkbox',
            'value' => ''
        ],
        'widthBlock2' => [
            'type' => 'select',
            'value' => '',
            'items' => [
                'fifty'=>'50%',
                'twenty-five'=>'25%',
                'rectangle'=>'rectangle'
            ]
        ],
        'posterBlock2' => [
            'type' => 'mediaInput',
            'value' => 'video/blocks/3INTERACTIVE-FLOOR-(LEVEL)-PLAN-WITH-APARTMENTS.jpg'
        ],
        'videoBlock2' => [
            'type' => 'mediaInput',
            'value' => 'video/blocks/480p/3INTERACTIVE-FLOOR-(LEVEL)-PLAN-WITH-APARTMENTS.mp'
        ],
        'linkBlock2' => [
            'type' => 'textInput',
            'value' => '#'
        ],
        'textBlock2' => [
            'type' => 'textarea',
            'value' => 'Interactive Floorplan with apartments'
        ],
        'activateBlock3' => [
            'type' => 'checkbox',
            'value' => ''
        ],
        'widthBlock3' => [
            'type' => 'select',
            'value' => '',
            'items' => [
                'fifty'=>'50%',
                'twenty-five'=>'25%',
                'rectangle'=>'rectangle'
            ]
        ],
        'posterBlock3' => [
            'type' => 'mediaInput',
            'value' => 'video/blocks/2WALKTHROUGH-MODE-IN-APARTMENT.jpg'
        ],
        'videoBlock3' => [
            'type' => 'mediaInput',
            'value' => 'video/blocks/480p/2WALKTHROUGH-MODE-IN-APARTMENT.mp4'
        ],
        'linkBlock3' => [
            'type' => 'textInput',
            'value' => '#'
        ],
        'textBlock3' => [
            'type' => 'textarea',
            'value' => 'Walktrough mode in apartment'
        ],
        'activateBlock4' => [
            'type' => 'checkbox',
            'value' => ''
        ],
        'widthBlock4' => [
            'type' => 'select',
            'value' => '',
            'items' => [
                'fifty'=>'50%',
                'twenty-five'=>'25%',
                'rectangle'=>'rectangle'
            ]
        ],
        'posterBlock4' => [
            'type' => 'mediaInput',
            'value' => 'video/blocks/5HOTSPOTS_1.jpg'
        ],
        'videoBlock4' => [
            'type' => 'mediaInput',
            'value' => 'video/blocks/480p/5HOTSPOTS_1.mp4'
        ],
        'linkBlock4' => [
            'type' => 'textInput',
            'value' => '#'
        ],
        'textBlock4' => [
            'type' => 'textarea',
            'value' => 'Hotspots'
        ],
        'activateBlock5' => [
            'type' => 'checkbox',
            'value' => ''
        ],
        'widthBlock5' => [
            'type' => 'select',
            'value' => '',
            'items' => [
                'fifty'=>'50%',
                'twenty-five'=>'25%',
                'rectangle'=>'rectangle'
            ]
        ],
        'posterBlock5' => [
            'type' => 'mediaInput',
            'value' => 'video/blocks/4FLYTHROUGH_2.jpg'
        ],
        'videoBlock5' => [
            'type' => 'mediaInput',
            'value' => 'video/blocks/480p/4FLYTHROUGH_2.mp4'
        ],
        'linkBlock5' => [
            'type' => 'textInput',
            'value' => '#'
        ],
        'textBlock5' => [
            'type' => 'textarea',
            'value' => 'Flythrough'
        ],
        'activateBlock6' => [
            'type' => 'checkbox',
            'value' => ''
        ],
        'widthBlock6' => [
            'type' => 'select',
            'value' => '',
            'items' => [
                'fifty'=>'50%',
                'twenty-five'=>'25%',
                'rectangle'=>'rectangle'
            ]
        ],
        'posterBlock6' => [
            'type' => 'mediaInput',
            'value' => 'video/blocks/6VIDEO-AND-GALLERY-INTEGRATION.jpg'
        ],
        'videoBlock6' => [
            'type' => 'mediaInput',
            'value' => 'video/blocks/480p/6VIDEO-AND-GALLERY-INTEGRATION.mp4'
        ],
        'linkBlock6' => [
            'type' => 'textInput',
            'value' => '#'
        ],
        'textBlock6' => [
            'type' => 'textarea',
            'value' => 'Video and gallery integration'
        ],
        'activateBlock7' => [
            'type' => 'checkbox',
            'value' => ''
        ],
        'widthBlock7' => [
            'type' => 'select',
            'value' => '',
            'items' => [
                'fifty'=>'50%',
                'twenty-five'=>'25%',
                'rectangle'=>'rectangle'
            ]
        ],
        'posterBlock7' => [
            'type' => 'mediaInput',
            'value' => 'video/blocks/7INTERACTIVE-MAP.jpg'
        ],
        'videoBlock7' => [
            'type' => 'mediaInput',
            'value' => 'video/blocks/480p/7INTERACTIVE-MAP.mp4'
        ],
        'linkBlock7' => [
            'type' => 'textInput',
            'value' => '#'
        ],
        'textBlock7' => [
            'type' => 'textarea',
            'value' => 'Interactive map'
        ],
        'activateBlockMoreWorks' => [
            'type' => 'checkbox',
            'value' => ''
        ],
        'widthBlockMoreWorks' => [
            'type' => 'select',
            'value' => '',
            'items' => [
                'fifty'=>'50%',
                'twenty-five'=>'25%',
                'rectangle'=>'rectangle'
            ]
        ],
        'linkBlockMoreWorks' => [
            'type' => 'textInput',
            'value' => '#'
        ],
        'textBlockMoreWorks' => [
            'type' => 'textarea',
            'value' => 'more works'
        ],
    ],
];
