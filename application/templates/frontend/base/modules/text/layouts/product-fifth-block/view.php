<?php
/**
 * File name: view.php
 *
 * @var $model \app\modules\text\models\Text;
 */

use yii\helpers\Url;
use app\modules\casecontent\models;

$isHome = (Yii::$app->request->baseUrl.'/index' == Url::to([''])) ? true : false;
?>

<div id="<?= $model->settings['id']['value'] ?>" class="portfolio-get-started-block work-get-started-block clearfix">
    <div class="pgb-content">
        <p class="pgb-text"><?= $model->settings['heading']['value'] ?></p>
        <span class="pgb-button anim-underline-hide"><?= $model->settings['button']['value'] ?></span>
    </div>
    <div class="pgb-form">
        <div class="pgb-white-line-wrapper">
            <div class="pgb-white-line"></div>
        </div>
        <div id="subscription">
            [form_builder id="<?= $model->settings['form']['value'] ?>"]
        </div>
    </div>
</div>