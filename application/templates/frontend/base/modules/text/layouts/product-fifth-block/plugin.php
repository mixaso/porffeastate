<?php
/**
 * File name: plugin.php
 */

return [
    'name' => 'product-fifth-block',
    'title' => Yii::t('text', 'Product Fifth Block'),
    'preview_image' => Yii::getAlias('@web').'/application/templates/frontend/base/modules/text/layouts/product-fifth-block/preview.png',
    'viewFile' => '@app/templates/frontend/base/modules/text/layouts/product-fifth-block/view.php',
    'settings' => [
        'id' => [
            'type' => 'textInput',
            'value' => ''
        ],
        'heading' => [
            'type' => 'textInput',
            'value' => 'Ready to work with us?'
        ],
        'button' => [
            'type' => 'textInput',
            'value' => 'GET STARTED'
        ],
        'form' => [
            'type' => 'formBuilder',
            'value' => ''
        ]
    ],
];