<?php
/**
 * File name: view.php
 *
 * @var $model \app\modules\text\models\Text;
 */

use yii\helpers\Url;

$isHome = (Yii::$app->request->baseUrl.'/index' == Url::to([''])) ? true : false;
?>

<div class="about-third-block clearfix">
    <div class="aside-left">
        <a class="content-facebook" href="<?= $model->settings['linkFacebook']['value'] ?>" target="_blank">Follow us</a>
    </div>
    <div class="content atb-content clearfix">
        <div class="c-text atb-text font-size"><?= $model->settings['text']['value'] ?>Our team excels at combining modern visulazation techniques into powerful interactive tools. If you feel up to making the world a more interactive and engaging place through technology, please join our guru touchscreens team.</div>
        <?php if($model->getSetting('person1', 0)):?>
            <div class="atb-photo">
                <div class="square-box">
                    <div class="square-content">
                        <img src="<?= $model->settings['person1Image']['value'] ?>" alt="<?= $model->settings['person1Alt']['value'] ?>">
                    </div>
                </div>
                <p class="atb-photo-name"><?= $model->settings['person1Name']['value'] ?></p>
                <p class="atb-photo-post"><?= $model->settings['person1Post']['value'] ?></p>
            </div>
        <?php endif; ?>

        <?php if($model->getSetting('person2', 0)):?>
            <div class="atb-photo">
                <div class="square-box">
                    <div class="square-content">
                        <img src="<?= $model->settings['person2Image']['value'] ?>" alt="<?= $model->settings['person2Alt']['value'] ?>">
                    </div>
                </div>
                <p class="atb-photo-name"><?= $model->settings['person2Name']['value'] ?></p>
                <p class="atb-photo-post"><?= $model->settings['person2Post']['value'] ?></p>
            </div>
        <?php endif; ?>

        <?php if($model->getSetting('person3', 0)):?>
            <div class="atb-photo">
                <div class="square-box">
                    <div class="square-content">
                        <img src="<?= $model->settings['person3Image']['value'] ?>" alt="<?= $model->settings['person3Alt']['value'] ?>">
                    </div>
                </div>
                <p class="atb-photo-name"><?= $model->settings['person3Name']['value'] ?></p>
                <p class="atb-photo-post"><?= $model->settings['person3Post']['value'] ?></p>
            </div>
        <?php endif; ?>

        <?php if($model->getSetting('person4', 0)):?>
            <div class="atb-photo">
                <div class="square-box">
                    <div class="square-content">
                        <img src="<?= $model->settings['person4Image']['value'] ?>" alt="<?= $model->settings['person4Alt']['value'] ?>">
                    </div>
                </div>
                <p class="atb-photo-name"><?= $model->settings['person4Name']['value'] ?></p>
                <p class="atb-photo-post"><?= $model->settings['person4Post']['value'] ?></p>
            </div>
        <?php endif; ?>

        <?php if($model->getSetting('person5', 0)):?>
            <div class="atb-photo">
                <div class="square-box">
                    <div class="square-content">
                        <img src="<?= $model->settings['person5Image']['value'] ?>" alt="<?= $model->settings['person5Alt']['value'] ?>">
                    </div>
                </div>
                <p class="atb-photo-name"><?= $model->settings['person5Name']['value'] ?></p>
                <p class="atb-photo-post"><?= $model->settings['person5Post']['value'] ?></p>
            </div>
        <?php endif; ?>

        <?php if($model->getSetting('person6', 0)):?>
            <div class="atb-photo">
                <div class="square-box">
                    <div class="square-content">
                        <img src="<?= $model->settings['person6Image']['value'] ?>" alt="<?= $model->settings['person6Alt']['value'] ?>">
                    </div>
                </div>
                <p class="atb-photo-name"><?= $model->settings['person6Name']['value'] ?></p>
                <p class="atb-photo-post"><?= $model->settings['person6Post']['value'] ?></p>
            </div>
        <?php endif; ?>
    </div>
</div>