<?php
/**
 * File name: plugin.php
 */

return [
    'name' => 'about-third-block',
    'title' => Yii::t('text', 'About Third Block'),
    'preview_image' => Yii::getAlias('@web').'/application/templates/frontend/base/modules/text/layouts/about-third-block/preview.png',
    'viewFile' => '@app/templates/frontend/base/modules/text/layouts/about-third-block/view.php',
    'settings' => [
        'linkFacebook' => [
            'type' => 'textInput',
            'value' => '//facebook.com'
        ],
        'text' => [
            'type' => 'textarea',
            'value' => 'Our team excels at combining modern visulazation techniques into powerful interactive tools. If you feel up to making the world a more interactive and engaging place through technology, please join our guru touchscreens team.'
        ],
        'person1' => [
            'type' => 'checkbox',
            'value' => ''
        ],
        'person1Image' => [
            'type' => 'mediaInput',
            'value' => 'images/people/gudova.jpg'
        ],
        'person1Alt' => [
            'type' => 'textInput',
            'value' => 'Gudova'
        ],
        'person1Name' => [
            'type' => 'textInput',
            'value' => 'Viktoria Gudova'
        ],
        'person1Post' => [
            'type' => 'textInput',
            'value' => 'CEO'
        ],
        'person2' => [
            'type' => 'checkbox',
            'value' => ''
        ],
        'person2Image' => [
            'type' => 'mediaInput',
            'value' => 'images/people/Aleksandrov@2x.jpg'
        ],
        'person2Alt' => [
            'type' => 'textInput',
            'value' => 'Aleksandrov'
        ],
        'person2Name' => [
            'type' => 'textInput',
            'value' => 'Dmitriy Aleksandrov'
        ],
        'person2Post' => [
            'type' => 'textInput',
            'value' => 'head of innovation'
        ],
        'person3' => [
            'type' => 'checkbox',
            'value' => ''
        ],
        'person3Image' => [
            'type' => 'mediaInput',
            'value' => 'images/people/Mikhno@2x.jpg" alt="Mikhno.jpg'
        ],
        'person3Alt' => [
            'type' => 'textInput',
            'value' => 'Mykhno'
        ],
        'person3Name' => [
            'type' => 'textInput',
            'value' => 'Victoria Mykhno'
        ],
        'person3Post' => [
            'type' => 'textInput',
            'value' => 'CCO'
        ],
        'person4' => [
            'type' => 'checkbox',
            'value' => ''
        ],
        'person4Image' => [
            'type' => 'mediaInput',
            'value' => 'images/people/goncharenko.jpg'
        ],
        'person4Alt' => [
            'type' => 'textInput',
            'value' => 'GONCHARENKO'
        ],
        'person4Name' => [
            'type' => 'textInput',
            'value' => 'IHOR GONCHARENKO'
        ],
        'person4Post' => [
            'type' => 'textInput',
            'value' => 'CTO'
        ],
        'person5' => [
            'type' => 'checkbox',
            'value' => ''
        ],
        'person5Image' => [
            'type' => 'mediaInput',
            'value' => 'images/people/rogach.jpg'
        ],
        'person5Alt' => [
            'type' => 'textInput',
            'value' => 'ROGACH'
        ],
        'person5Name' => [
            'type' => 'textInput',
            'value' => 'Sergei Rogach'
        ],
        'person5Post' => [
            'type' => 'textInput',
            'value' => 'Head of PM department'
        ],
        'person6' => [
            'type' => 'checkbox',
            'value' => ''
        ],
        'person6Image' => [
            'type' => 'mediaInput',
            'value' => 'images/people/nashilova.jpg'
        ],
        'person6Alt' => [
            'type' => 'textInput',
            'value' => 'SNASHILOVA'
        ],
        'person6Name' => [
            'type' => 'textInput',
            'value' => 'SNASHILOVA ELENA'
        ],
        'person6Post' => [
            'type' => 'textInput',
            'value' => 'Head if UX/UI'
        ],
    ],
];