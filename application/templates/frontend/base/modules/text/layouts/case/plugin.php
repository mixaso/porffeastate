<?php
/**
 * File name: plugin.php
 */

return [
    'name' => 'case',
    'title' => Yii::t('text', 'Case'),
    'preview_image' => Yii::getAlias('@web').'/application/templates/frontend/base/modules/text/layouts/case/preview.png',
    'viewFile' => '@app/templates/frontend/base/modules/text/layouts/case/view.php',
    'settings' => [
        'enableTopImg' => [
            'type' => 'checkbox',
            'value' => ''
        ],
        'topImg' => [
            'type' => 'mediaInput',
            'value' => ''
        ],
        'ttl' => [
            'type' => 'textInput',
            'value' => ''
        ],
        'logo' => [
            'type' => 'mediaInput',
            'value' => ''
        ],
        'logoAlt' => [
            'type' => 'textInput',
            'value' => ''
        ],
        'enableFirst' => [
            'type' => 'checkbox',
            'value' => ''
        ],
        'firstText' => [
            'type' => 'textarea',
            'value' => ''
        ],
        'enableSecond' => [
            'type' => 'checkbox',
            'value' => ''
        ],
        'secondTextTitle' => [
            'type' => 'textInput',
            'value' => ''
        ],
        'secondText' => [
            'type' => 'textarea',
            'value' => ''
        ],
        'enableSecondVideo' => [
            'type' => 'checkbox',
            'value' => ''
        ],
        'secondMedia' => [
            'type' => 'mediaInput',
            'value' => ''
        ],
        'secondMediaPoster' => [
            'type' => 'mediaInput',
            'value' => ''
        ],
        'enableSecondIFrame' => [
            'type' => 'checkbox',
            'value' => ''
        ],
        'secondFrameLink' => [
            'type' => 'textInput',
            'value' => ''
        ],
        'enableThird' => [
            'type' => 'checkbox',
            'value' => ''
        ],
        'thirdTextTitle' => [
            'type' => 'textInput',
            'value' => ''
        ],
        'thirdText' => [
            'type' => 'textarea',
            'value' => ''
        ],
        'thirdMedia' => [
            'type' => 'mediaInput',
            'value' => ''
        ],
        'thirdMediaAlt' => [
            'type' => 'textInput',
            'value' => ''
        ],
        'enableFourth' => [
            'type' => 'checkbox',
            'value' => ''
        ],
        'fourthTextTitle' => [
            'type' => 'textInput',
            'value' => ''
        ],
        'fourthText' => [
            'type' => 'textarea',
            'value' => ''
        ],
        'fourthMedia1' => [
            'type' => 'mediaInput',
            'value' => ''
        ],
        'fourthMedia1Alt' => [
            'type' => 'textInput',
            'value' => ''
        ],
        'fourthMedia2' => [
            'type' => 'mediaInput',
            'value' => ''
        ],
        'fourthMedia2Alt' => [
            'type' => 'textInput',
            'value' => ''
        ],
        'fourthMedia3' => [
            'type' => 'mediaInput',
            'value' => ''
        ],
        'fourthMedia3Alt' => [
            'type' => 'textInput',
            'value' => ''
        ],
        'fourthMedia4' => [
            'type' => 'mediaInput',
            'value' => ''
        ],
        'fourthMedia4Alt' => [
            'type' => 'textInput',
            'value' => ''
        ],
        'enableFifth' => [
            'type' => 'checkbox',
            'value' => ''
        ],
        'fifthTextTitle' => [
            'type' => 'textInput',
            'value' => ''
        ],
        'fifthText' => [
            'type' => 'textarea',
            'value' => ''
        ],
        'fifthWidget360View' => [
            'type' => 'widgetkit',
            'value' => ''
        ],
        'fifthWidget360ViewText' => [
            'type' => 'textInput',
            'value' => '360 View'
        ],
        'fifthWidget3dRenders' => [
            'type' => 'widgetkit',
            'value' => ''
        ],
        'fifthWidget3dRendersText' => [
            'type' => 'textInput',
            'value' => '3D renders'
        ],
        'fifthMediaBackground' => [
            'type' => 'mediaInput',
            'value' => ''
        ],
        'fifthMedia' => [
            'type' => 'mediaInput',
            'value' => ''
        ],
        'fifthMediaAlt' => [
            'type' => 'textInput',
            'value' => ''
        ],
        'enableSixth' => [
            'type' => 'checkbox',
            'value' => ''
        ],
        'sixthTextTitle' => [
            'type' => 'textInput',
            'value' => ''
        ],
        'sixthText' => [
            'type' => 'textarea',
            'value' => ''
        ],
        'sixthWidgetApartments' => [
            'type' => 'widgetkit',
            'value' => ''
        ],
        'sixthWidgetApatrmentsText' => [
            'type' => 'textInput',
            'value' => 'Apartments'
        ],
        'sixthTabName1' => [
            'type' => 'textInput',
            'value' => ''
        ],
        'sixthTabTitle1' => [
            'type' => 'textInput',
            'value' => ''
        ],
        'sixthTabText1' => [
            'type' => 'textarea',
            'value' => ''
        ],
        'sixthTabMedia1' => [
            'type' => 'mediaInput',
            'value' => ''
        ],
        'sixthTabName2' => [
            'type' => 'textInput',
            'value' => ''
        ],
        'sixthTabTitle2' => [
            'type' => 'textInput',
            'value' => ''
        ],
        'sixthTabText2' => [
            'type' => 'textarea',
            'value' => ''
        ],
        'sixthTabMedia2' => [
            'type' => 'mediaInput',
            'value' => ''
        ],
        'sixthTabName3' => [
            'type' => 'textInput',
            'value' => ''
        ],
        'sixthTabTitle3' => [
            'type' => 'textInput',
            'value' => ''
        ],
        'sixthTabText3' => [
            'type' => 'textarea',
            'value' => ''
        ],
        'sixthTabMedia3' => [
            'type' => 'mediaInput',
            'value' => ''
        ],
        'sixthTabName4' => [
            'type' => 'textInput',
            'value' => ''
        ],
        'sixthTabTitle4' => [
            'type' => 'textInput',
            'value' => ''
        ],
        'sixthTabText4' => [
            'type' => 'textarea',
            'value' => ''
        ],
        'sixthTabMedia4' => [
            'type' => 'mediaInput',
            'value' => ''
        ],
        'enableSeventh' => [
            'type' => 'checkbox',
            'value' => ''
        ],
        'seventhTextTitle' => [
            'type' => 'textInput',
            'value' => ''
        ],
        'seventhText' => [
            'type' => 'textarea',
            'value' => ''
        ],
        'seventhMediaLeft' => [
            'type' => 'mediaInput',
            'value' => ''
        ],
        'seventhMediaLeftAlt' => [
            'type' => 'textInput',
            'value' => ''
        ],
        'seventhMediaRight' => [
            'type' => 'mediaInput',
            'value' => ''
        ],
        'seventhMediaRightAlt' => [
            'type' => 'mediaInput',
            'value' => ''
        ],
        'enableEighth' => [
            'type' => 'checkbox',
            'value' => ''
        ],
        'eighthTextTitle' => [
            'type' => 'textInput',
            'value' => ''
        ],
        'eighthText' => [
            'type' => 'textarea',
            'value' => ''
        ],
        'eighthMediaBackground' => [
            'type' => 'mediaInput',
            'value' => ''
        ],
        'eighthMedia' => [
            'type' => 'mediaInput',
            'value' => ''
        ],
        'eighthMediaAlt' => [
            'type' => 'mediaInput',
            'value' => ''
        ],
        'enableForm' => [
            'type' => 'checkbox',
            'value' => ''
        ],
        'enableWorks' => [
            'type' => 'checkbox',
            'value' => ''
        ],
        'moreWorkText' => [
            'type' => 'textInput',
            'value' => '+ works'
        ],
    ],
];