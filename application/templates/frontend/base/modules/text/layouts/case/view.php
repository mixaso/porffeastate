<?php
/**
 * File name: view.php
 *
 * @var $model \app\modules\text\models\Text;
 */

use yii\helpers\Url;
use app\modules\casecontent\models\ContentArticles;
use app\modules\casecontent\models\ContentCategoryLang;
use app\modules\casecontent\models\ContentTags;
use app\modules\casecontent\models\ContentTagAssn;
use yii\helpers\Html;

$this->params['breadcrumbs'][] = [
    'url'   => ['/cases'],
    'label' => 'Cases',
];
$menu = Yii::$app->menuManager->getActiveMenu();
$this->params['breadcrumbs'][] = $model->settings['ttl']['value'];
//echo '<pre>';print_r($this->params['breadcrumbs']);echo '</pre>';
$isHome = (Yii::$app->request->baseUrl.'/index' == Url::to([''])) ? true : false;
?>

<div class="caseItem">
    <?php if($model->getSetting('enableTopImg', 0)):?>
        <div class="caseItem_topImg" style="background: url('<?= $model->settings['topImg']['value'] ?>') center;
                background-size: cover;"></div>
    <?php endif; ?>

    <div class="caseItem__heading">
        <?php if(isset($this->params['breadcrumbs'])): ?>
            <div class="breadcrumbs caseItem_breadcrumbs pull-left">
                <?= \yii\widgets\Breadcrumbs::widget([
                    'options' => ['class' => 'inline-layout'],
                    'itemTemplate' => "<li>{link}<span>/</span></li>\n",
                    'activeItemTemplate' => "<li><span>{link}</span></li>\n",
                    'links' => $this->params['breadcrumbs'],
                    'homeLink' => false,
                ]);?>
            </div>
            <?php $this->params['breadcrumbs'] = null; ?>
        <?php endif; ?>
        <div class="pull-right">
            <div class="caseItem__heading_ttl">
                <?= $model->settings['ttl']['value'] ?>
            </div>
            <div class="caseItem__heading_logo">
                <img src="<?= $model->settings['logo']['value'] ?>" alt="<?= $model->settings['logoAlt']['value'] ?>">
            </div>
        </div>
    </div>
    <div class="row">
        <?php if($model->getSetting('enableFirst', 0)):?>
            <div class="caseItem__first clearfix">
                <div class="col-lg-9 col-lg-offset-3 caseItem__first_text clearfix">
                    <div class="caseItem__first_text_in">
                        <span>
                            <?= $model->settings['firstText']['value'] ?>
                        </span>
                    </div>
                </div>
            </div>
        <?php endif; ?>

        <?php if($model->getSetting('enableSecond', 0)):?>
            <div class="caseItem__second clearfix">
                <div class="col-lg-9 col-lg-push-3 caseItem__second_media works-block wwb-content clearfix">
                    <div class="top-home-slider portfolio-slider">
                        <div class="th-slider-item-wrapper">
                            <div class="th-slider-item" style="padding-bottom: 0;">
                                <?php if($model->getSetting('enableSecondVideo', 0)):?>
                                    <video class="" poster="<?= $model->settings['secondMediaPoster']['value'] ?>" preload="auto">
                                        <source type="video/mp4" src="<?= $model->settings['secondMedia']['value'] ?>"/>
                                        <p>Your user agent does not support the HTML5 Video element.</p>
                                    </video>
                                    <span class="video-play-button"></span>
                                <?php endif; ?>

                                <?php if($model->getSetting('enableSecondIFrame', 0)):?>
                                    <iframe width="100%" height="100%" src="<?= $model->settings['secondFrameLink']['value'] ?>" frameborder="0" allowfullscreen></iframe>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-lg-pull-9 caseItem__second_text">
                    <h3>
                        <?= $model->settings['secondTextTitle']['value'] ?>
                    </h3>
                    <span>
                    <?= $model->settings['secondText']['value'] ?>
                </span>
                </div>
            </div>
        <?php endif; ?>

        <?php if($model->getSetting('enableThird', 0)):?>
            <div class="caseItem__third clearfix">
                <div class="col-lg-9 col-lg-push-3 caseItem__third_media">
                    <img src="<?= $model->settings['thirdMedia']['value'] ?>" alt="<?= $model->settings['thirdMediaAlt']['value'] ?>" class="caseItem__third_media_monitor">
                    <img src="images/cases/women.png" alt="" class="caseItem__third_media_women">
                    <img src="images/cases/man.png" alt="" class="caseItem__third_media_man">
                </div>
                <div class="col-lg-3 col-lg-pull-9 caseItem__third_text">
                    <h3>
                        <?= $model->settings['thirdTextTitle']['value'] ?>
                    </h3>
                    <span>
                    <?= $model->settings['thirdText']['value'] ?>
                </span>
                </div>
            </div>
        <?php endif; ?>

        <?php if($model->getSetting('enableFourth', 0)):?>
            <div class="caseItem__fourth clearfix">
                <div class="col-lg-9 col-lg-push-3 caseItem__fourth_media">
                    <img src="<?= $model->settings['fourthMedia1']['value'] ?>" class="caseItem__fourth_media_1" alt="<?= $model->settings['fourthMedia1Alt']['value'] ?>">
                    <img src="<?= $model->settings['fourthMedia2']['value'] ?>" class="caseItem__fourth_media_2" alt="<?= $model->settings['fourthMedia2Alt']['value'] ?>">
                    <img src="<?= $model->settings['fourthMedia3']['value'] ?>" class="caseItem__fourth_media_3" alt="<?= $model->settings['fourthMedia3Alt']['value'] ?>">
                    <img src="<?= $model->settings['fourthMedia4']['value'] ?>" class="caseItem__fourth_media_4" alt="<?= $model->settings['fourthMedia4Alt']['value'] ?>">
                    <div class="caseItem__fourth_media_l1"></div>
                    <div class="caseItem__fourth_media_l2"></div>
                    <div class="caseItem__fourth_media_l3"></div>
                    <div class="caseItem__fourth_media_start"></div>
                </div>
                <div class="col-lg-3 col-lg-pull-9 caseItem__fourth_text">
                    <h3>
                        <?= $model->settings['fourthTextTitle']['value'] ?>
                    </h3>
                    <span>
                    <?= $model->settings['fourthText']['value'] ?>
                </span>
                </div>
            </div>
        <?php endif; ?>

        <?php if($model->getSetting('enableFifth', 0)):?>
            <div class="caseItem__fifth clearfix">
                <div class="col-lg-9 col-lg-push-3 caseItem__fifth_media">
                    <img src="<?= $model->settings['fifthMediaBackground']['value'] ?>" class="caseItem__fifth_media_bg"
                         alt="">
                    <div class="caseItem__fifth_media_bg-in">
                    </div>
                    <img src="<?= $model->settings['fifthMedia']['value'] ?>" alt="<?= $model->settings['fifthMediaAlt']['value'] ?>" class="caseItem__fifth_media_img">
                </div>
                <div class="col-lg-3 col-lg-pull-9 caseItem__fifth_text">
                    <h3>
                        <?= $model->settings['fifthTextTitle']['value'] ?>
                    </h3>
                    <span>
                    <?= $model->settings['fifthText']['value'] ?>
                </span>
                    <div class="caseItem__fifth_text_link">
                        <button id="360view" class="uk-button" data-uk-modal="{target: '#<?= $model->id ?>360view'}">
                            <?= $model->settings['fifthWidget360ViewText']['value'] ?>
                        </button>
                    </div>
                    <div class="caseItem__fifth_text_link">
                        <button class="uk-button" data-uk-modal="{target: '#<?= $model->id ?>3drenders'}">
                            <?= $model->settings['fifthWidget3dRendersText']['value'] ?>
                        </button>
                    </div>
                </div>
            </div>
        <?php endif; ?>

        <?php if($model->getSetting('enableSixth', 0)):?>
            <div class="caseItem__sixth clearfix">
                <div class="caseItem__sixth_in">
                    <div class="col-lg-3 col-lg-push-9 caseItem__sixth_tabs">
                        <!-- Nav tabs -->
                        <ul id="caseTabs" class="nav nav-tabs nav-stacked" role="tablist">
                            <li role="presentation" class="caseItem__sixth_tabs_first active">
                                <a href="<?php echo Url::to() ?>#<?= $model->settings['sixthTabName1']['value'] ?>" role="tab" data-toggle="tab" data-target="#<?= $model->settings['sixthTabName1']['value'] ?>, #<?= $model->settings['sixthTabName1']['value'] ?>2">
                                    <?= $model->settings['sixthTabTitle1']['value'] ?>
                                </a>

                            </li>
                            <li role="presentation" class="caseItem__sixth_tabs_second">
                                <a href="<?php echo Url::to() ?>#<?= $model->settings['sixthTabName2']['value'] ?>" role="tab" data-toggle="tab" data-target="#<?= $model->settings['sixthTabName2']['value'] ?>, #<?= $model->settings['sixthTabName2']['value'] ?>2">
                                    <?= $model->settings['sixthTabTitle2']['value'] ?>
                                </a>
                            </li>
                            <li role="presentation" class="caseItem__sixth_tabs_third">
                                <a href="<?php echo Url::to() ?>#<?= $model->settings['sixthTabName3']['value'] ?>" role="tab" data-toggle="tab" data-target="#<?= $model->settings['sixthTabName3']['value'] ?>, #<?= $model->settings['sixthTabName3']['value'] ?>2">
                                    <?= $model->settings['sixthTabTitle3']['value'] ?>
                                </a>
                            </li>
                            <li role="presentation" class="caseItem__sixth_tabs_fourth">
                                <a href="<?php echo Url::to() ?>#<?= $model->settings['sixthTabName4']['value'] ?>" role="tab" data-toggle="tab" data-target="#<?= $model->settings['sixthTabName4']['value'] ?>, #<?= $model->settings['sixthTabName4']['value'] ?>2">
                                    <?= $model->settings['sixthTabTitle4']['value'] ?>
                                </a>
                            </li>
                        </ul>
                    </div>
                    <div class="col-lg-3 col-lg-pull-3 caseItem__sixth_text">
                        <h3>
                            <?= $model->settings['sixthTextTitle']['value'] ?>
                        </h3>
                        <span>
                    <?= $model->settings['sixthText']['value'] ?>
                </span>

                        <!-- Tab panes -->
                        <div class="tab-content caseItem__sixth_text_tab">
                            <div role="tabpanel" class="tab-pane fade in active" id="<?= $model->settings['sixthTabName1']['value']
                            ?>2">
                                <?= $model->settings['sixthTabText1']['value'] ?>
                            </div>
                            <div role="tabpanel" class="tab-pane fade" id="<?= $model->settings['sixthTabName2']['value'] ?>2">
                                <?= $model->settings['sixthTabText2']['value'] ?>
                            </div>
                            <div role="tabpanel" class="tab-pane fade" id="<?= $model->settings['sixthTabName3']['value'] ?>2">
                                <?= $model->settings['sixthTabText3']['value'] ?>
                            </div>
                            <div role="tabpanel" class="tab-pane fade" id="<?= $model->settings['sixthTabName4']['value'] ?>2">
                                <?= $model->settings['sixthTabText4']['value'] ?>
                            </div>
                        </div>

                        <div class="caseItem__sixth_text_link">
                            <button class="uk-button" data-uk-modal="{target: '#<?= $model->id ?>Apartments'}">
                                <?= $model->settings['sixthWidgetApatrmentsText']['value'] ?>
                            </button>
                        </div>
                    </div>
                    <div class="col-lg-6 col-lg-pull-3 caseItem__sixth_media">
                        <!-- Tab panes -->
                        <div class="tab-content">
                            <div role="tabpanel" class="tab-pane fade in active" id="<?= $model->settings['sixthTabName1']['value']
                            ?>">
                                <img src="<?= $model->settings['sixthTabMedia1']['value'] ?>" alt="">
                            </div>
                            <div role="tabpanel" class="tab-pane fade" id="<?= $model->settings['sixthTabName2']['value'] ?>">
                                <img src="<?= $model->settings['sixthTabMedia2']['value'] ?>" alt="">
                            </div>
                            <div role="tabpanel" class="tab-pane fade" id="<?= $model->settings['sixthTabName3']['value'] ?>">
                                <img src="<?= $model->settings['sixthTabMedia3']['value'] ?>" alt="">
                            </div>
                            <div role="tabpanel" class="tab-pane fade" id="<?= $model->settings['sixthTabName4']['value'] ?>">
                                <img src="<?= $model->settings['sixthTabMedia4']['value'] ?>" alt="">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <?php endif; ?>

        <?php if($model->getSetting('enableSeventh', 0)):?>
            <div class="caseItem__seventh clearfix">
                <div class="col-lg-9 col-lg-push-3 caseItem__seventh_media clearfix">
                    <div class="caseItem__seventh_media_left">
                        <img src="<?= $model->settings['seventhMediaLeft']['value'] ?>" alt="<?= $model->settings['seventhMediaLeftAlt']['value'] ?>" >
                    </div>
                    <div class="caseItem__seventh_media_right">
                        <img src="<?= $model->settings['seventhMediaRight']['value'] ?>" alt="<?= $model->settings['seventhMediaRightAlt']['value'] ?>">
                    </div>
                    <div class="caseItem__seventh_media_start"></div>
                </div>
                <div class="col-lg-3 col-lg-pull-9 caseItem__seventh_text">
                    <h3>
                        <?= $model->settings['seventhTextTitle']['value'] ?>
                    </h3>
                    <span>
                    <?= $model->settings['seventhText']['value'] ?>
                </span>
                </div>
            </div>
        <?php endif; ?>

        <?php if($model->getSetting('enableEighth', 0)):?>
            <div class="caseItem__eighth clearfix">
                <div class="col-lg-9 col-lg-push-3 caseItem__eighth_media">
                    <img src="<?= $model->settings['eighthMediaBackground']['value'] ?>" class="caseItem__eighth_media_bg" alt="">
                    <div class="caseItem__eighth_media_bg-in">
                    </div>
                    <img src="<?= $model->settings['eighthMedia']['value'] ?>" alt="<?= $model->settings['eighthMediaAlt']['value'] ?>" class="caseItem__eighth_media_img">
                </div>
                <div class="col-lg-3 col-lg-pull-9 caseItem__eighth_text">
                    <h3>
                        <?= $model->settings['eighthTextTitle']['value'] ?>
                    </h3>
                    <span>
                    <?= $model->settings['eighthText']['value'] ?>
                </span>
                </div>
            </div>
        <?php endif; ?>

        <?php if($model->getSetting('enableForm', 0)):?>
            [block id='27']
        <?php endif; ?>

        <?php if($model->getSetting('enableWorks', 0)): ?>
            <div>
                <section class="case__items">
            <?php /*vmazuryk start custom cases*/

            $curcase = Yii::$app->getRequest()->get('slug');
            $lang = strtolower(Yii::$app->language);
            $catid = ContentArticles::find()
                     ->select('oak_case_content_articles.category_id')
                     ->leftJoin('oak_case_content_articles_lang', '`oak_case_content_articles_lang`.`content_articles_id` = `oak_case_content_articles`.`id`')
                     ->andWhere(['oak_case_content_articles_lang.language'=>$lang])
                     ->andWhere(['=','oak_case_content_articles_lang.slug',$curcase])
                     ->asArray()
                     ->one();

            $field = \app\modules\field\models\FieldValue::find()->where(['value'=>'comingsoon'])->select('item_id')->all();
            foreach ($field as $fil){
                $infield []= $fil->item_id;
            }

            $allcases = ContentArticles::find()
                        ->select(['oak_case_content_articles_lang.*','oak_case_content_articles.*'])
                        ->leftJoin('oak_case_content_articles_lang', '`oak_case_content_articles_lang`.`content_articles_id` = `oak_case_content_articles`.`id`')
                        ->andWhere(['oak_case_content_articles_lang.language'=>$lang])
                        ->andWhere(['<>','oak_case_content_articles_lang.slug',$curcase])
                        ->andWhere(['oak_case_content_articles.category_id'=>$catid['category_id']])
                        ->andWhere(['not in','oak_case_content_articles.id',$infield])
                        ->all();

            shuffle($allcases);
            $allcases = array_slice($allcases, 0,3); ?>
           <?php
                 foreach ($allcases as $case): ?>
                        <div class="col-sm-3 case__item" data-key="2">
                            <div class="">
                                <a href="/cases/<?= $case->slug ?>" style='background-image: url("/uploads/news/<?= $case->id; ?>/thumb/thumb-<?= $case->image; ?>"); background-size: cover;
                                        background-position: center;'>
                                    <div class="case__item_img" style="height: 337px;">
                                        <div class="case__item__cntnt">
                                            <div class="case__item__cntnt_ttl">
                                                <span><?= $case->title ?></span>
                                            </div>
                                            <div class="case__item__cntnt_content">
                                                <p><?= $case->description ?></p>
                                            </div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>
            <?php endforeach; ?>

            <?php
            $allsoon = ContentArticles::find()
                       ->select(['oak_case_content_articles_lang.*','oak_case_content_articles.*'])
                       ->leftJoin('oak_case_content_articles_lang', '`oak_case_content_articles_lang`.`content_articles_id` = `oak_case_content_articles`.`id`')
                       ->andWhere(['oak_case_content_articles_lang.language'=>$lang])
                       ->andWhere(['<>','oak_case_content_articles_lang.slug',$curcase])
                       ->andWhere(['oak_case_content_articles.category_id'=>$catid['category_id']])
                       ->andWhere(['in','oak_case_content_articles.id',$infield])
                       ->all();

            shuffle($allsoon);
            $outcooming = false;
            if (count($allcases) == 1) {
                $allsoon = array_slice($allsoon, 0, 2);
                $outcooming = true;
            }
            if (count($allcases) == 2) {
                $allsoon = array_slice($allsoon, 0, 1);
                $outcooming = true;
            }
            if (count($allcases) >= 3) {
                $outcooming = false;
            }
            ?>
              <?php
              if ($outcooming):
                  foreach ($allsoon as $soon): ?>
                        <div class="col-sm-3 case__item" data-key="2">
                            <div class="comingsoon">
                                <a href="" style='background-image: url("/uploads/news/<?= $soon->id; ?>/thumb/thumb-<?= $soon->image; ?>"); background-size: cover;
                                        background-position: center;'>
                                    <div class="case__item_img" style="height: 337px;">
                                        <div class="case__item__cntnt">
                                            <div class="case__item__cntnt_ttl">
                                                <span><?= $soon->title ?></span>
                                            </div>
                                            <div class="case__item__cntnt_content">
                                                <p><?= $soon->description ?></p>
                                            </div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>
            <?php endforeach;
              endif;/*vmazuryk end custom cases*/
            ?>

                    <div class="col-sm-3 case__item" data-key="2">
                        <div class="case__item_img more-works">
                            <?php
                            $aacatid = ContentCategoryLang::find()->select('slug')->where(['id'=>$catid['category_id']])->one();
                            ?>
                            <?= Html::a($model->settings['moreWorkText']['value'], ['/casecontent/category/view', 'slug' => $aacatid['slug'] ])?>
                        </div>
                    </div>
                </section>
            </div>
        <?php endif; ?>

    </div>

    <div class="modal fade" id="<?= $model->getSetting('id') ?>" tabindex="-1" role="dialog" aria-labelledby="<?= $model->getSetting('id') ?>">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <div class="modal-body">
                    <?= $model->getSetting('frame') ?>
                </div>
            </div>
        </div>
    </div>

    <div id="<?= $model->id ?>3drenders" class="uk-modal">
        [widgetkit id='<?= $model->getSetting('fifthWidget3dRenders') ?>']
    </div>

    <div id="<?= $model->id ?>Apartments" class="uk-modal">
        [widgetkit id='<?= $model->getSetting('sixthWidgetApartments') ?>']
    </div>

    <div id="<?= $model->id ?>360view" class="uk-modal">
        [widgetkit id='<?= $model->getSetting('fifthWidget360View') ?>']
    </div>
</div>
</div>