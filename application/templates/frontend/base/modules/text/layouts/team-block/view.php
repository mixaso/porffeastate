<?php
/**
 * File name: view.php
 *
 * @var $model \app\modules\text\models\Text;
 */

use yii\helpers\Url;

$isHome = (Yii::$app->request->baseUrl.'/index' == Url::to([''])) ? true : false;
?>

<div class="team-block clearfix">

    <div class="team-block-content">
        <div class="team-block-terminal-wrapper box-square">
            <div class="team-block-terminal square-content">
                <img src="<?= $model->settings['rightImage']['value'] ?>" alt="<?= $model->settings['rightImageAlt']['value'] ?>">
            </div>
        </div>
        <div class="team-block-text font-size">
            <span><?= $model->settings['textIn']['value'] ?></span>
        </div>
        <div class="team-block-face-wrapper box-square">
            <div class="team-block-face square-content">
                <?php if($model->getSetting('activateImage1', 0)):?>
                    <img src="<?= $model->settings['image1']['value'] ?>" alt="<?= $model->settings['image1Alt']['value'] ?>" />
                <?php endif; ?>

                <?php if($model->getSetting('activateImage2', 0)):?>
                    <img src="<?= $model->settings['image2']['value'] ?>" alt="<?= $model->settings['image2Alt']['value'] ?>" />
                <?php endif; ?>

                <?php if($model->getSetting('activateImage3', 0)):?>
                    <img src="<?= $model->settings['image3']['value'] ?>" alt="<?= $model->settings['image3Alt']['value'] ?>" />
                <?php endif; ?>

                <?php if($model->getSetting('activateImage4', 0)):?>
                    <img src="<?= $model->settings['image4']['value'] ?>" alt="<?= $model->settings['image4Alt']['value'] ?>" />
                <?php endif; ?>

                <?php if($model->getSetting('activateImage5', 0)):?>
                    <img src="<?= $model->settings['image5']['value'] ?>" alt="<?= $model->settings['image5Alt']['value'] ?>" />
                <?php endif; ?>

                <?php if($model->getSetting('activateImage6', 0)):?>
                    <img src="<?= $model->settings['image6']['value'] ?>" alt="<?= $model->settings['image6Alt']['value'] ?>" />
                <?php endif; ?>
            </div>
        </div>
    </div>
    <div class="team-block-link-wrapper">
        <span class="font-size"><?= $model->settings['textAfter']['value'] ?></span>
    </div>
</div>