<?php
/**
 * File name: plugin.php
 */

return [
    'name' => 'team-block',
    'title' => Yii::t('text', 'Team Block'),
    'preview_image' => Yii::getAlias('@web').'/application/templates/frontend/base/modules/text/layouts/team-block/preview.png',
    'viewFile' => '@app/templates/frontend/base/modules/text/layouts/team-block/view.php',
    'settings' => [
        'rightImage' => [
            'type' => 'mediaInput',
            'value' => 'images/terminal@2x.png'
        ],
        'rightImageAlt' => [
            'type' => 'textInput',
            'value' => ''
        ],
        'textIn' => [
            'type' => 'textarea',
            'value' => 'More about <a href="/product">the product</a>'
        ],
        'activateImage1' => [
            'type' => 'checkbox',
            'value' => ''
        ],
        'image1' => [
            'type' => 'mediaInput',
            'value' => 'images/people/gudova.jpg'
        ],
        'image1Alt' => [
            'type' => 'textInput',
            'value' => ''
        ],
        'activateImage2' => [
            'type' => 'checkbox',
            'value' => ''
        ],
        'image2' => [
            'type' => 'mediaInput',
            'value' => 'images/people/Aleksandrov@2x.jpg'
        ],
        'image2Alt' => [
            'type' => 'textInput',
            'value' => ''
        ],
        'activateImage3' => [
            'type' => 'checkbox',
            'value' => ''
        ],
        'image3' => [
            'type' => 'mediaInput',
            'value' => 'images/people/Mikhno@2x.jpg'
        ],
        'image3Alt' => [
            'type' => 'textInput',
            'value' => ''
        ],
        'activateImage4' => [
            'type' => 'checkbox',
            'value' => ''
        ],
        'image4' => [
            'type' => 'mediaInput',
            'value' => 'images/people/goncharenko.jpg'
        ],
        'image4Alt' => [
            'type' => 'textInput',
            'value' => ''
        ],
        'activateImage5' => [
            'type' => 'checkbox',
            'value' => ''
        ],
        'image5' => [
            'type' => 'mediaInput',
            'value' => 'images/people/rogach.jpg'
        ],
        'image5Alt' => [
            'type' => 'textInput',
            'value' => ''
        ],
        'activateImage6' => [
            'type' => 'checkbox',
            'value' => ''
        ],
        'image6' => [
            'type' => 'mediaInput',
            'value' => 'images/people/nashilova.jpg'
        ],
        'image6Alt' => [
            'type' => 'textInput',
            'value' => ''
        ],
        'textAfter' => [
            'type' => 'textarea',
            'value' => 'More about <a class="anim-underline-hide" href="/about">the team</a>'
        ],
    ],
];
