<?php
/**
 * File name: view.php
 *
 * @var $model \app\modules\text\models\Text;
 */

use yii\helpers\Url;
use app\modules\menu\api\Menu as ApiMenu;

$isHome = (Yii::$app->request->baseUrl.'/index' == Url::to([''])) ? true : false;
?>

<header>
    <a class="logo" href="/">
        <img alt="<?= $model->settings['logoAlt']['value'] ?>" src="<?= $model->settings['logo']['value'] ?>" />
    </a>
    <div class="header-loader">
        <a class="mobile-menu-btn" href="javascript:void(0)"></a>
    </div>
    <ul class="h-top-menu">
        <li>
            <?php
            $items = ApiMenu::getMenuLvl($model->getSetting('menuHome'), 1, 2);
            echo \app\modules\system\components\Menu::widget([
                'items' => $items,
                'options' => [
                    'class' => 'h-top-menu in'
                ]
            ]);
            //            echo Menu::widget()
            ?>
            <?php
            $lang = Yii::$app->request->preferredLanguage;
            $alllang = \app\modules\language\models\Language::getLanguages();
            $lang_main = [];
            $lang_second = [];
            foreach ($alllang as $ll){
                if ($ll->language_id == $lang){
                    $lang_main[] = $ll->language_id;
                }
                else {
                    $lang_second[] = $ll->language_id;
                }
            }
            ?>
        </li>
        <li class="h-top-menu-item first-level-item language">
            <a class="h-top-menu-link first-level-item-link" href="<?= Url::toRoute(['/', '__language' => substr($lang_main[0],0,2)]) ?>"><?= substr($lang_main[0],3); ?></a>
            <ul class="h-top-menu-second-level">
                <?php for ($i=0; $i < count($lang_second); $i++):?>
                <li class="h-top-menu-second-level-item second-level-item">
                    <a class="h-top-menu-link second-level-item-link" href="<?= Url::toRoute(['/', '__language' => substr($lang_second[$i],0,2)])?>"><?= strtoupper(substr($lang_second[$i],0,2)); ?></a>
                </li>
                <?php endfor;?>
            </ul>
        </li>
    </ul>
</header>