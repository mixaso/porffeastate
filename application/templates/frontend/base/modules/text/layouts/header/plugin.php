<?php
/**
 * File name: plugin.php
 */

return [
    'name' => 'header',
    'title' => Yii::t('text', 'Header'),
    'preview_image' => Yii::getAlias('@web').'/application/templates/frontend/base/modules/text/layouts/header/preview.png',
    'viewFile' => '@app/templates/frontend/base/modules/text/layouts/header/view.php',
    'settings' => [
        'logo' => [
            'type' => 'mediaInput',
            'value' => 'images/logo.png'
        ],
        'logoAlt' => [
            'type' => 'textInput',
            'value' => ''
        ],
        'menuHome' => [
            'type' => 'menuType',
            'value' => '5'
        ]
    ],
];
