<?php
/**
 * File name: plugin.php
 */

return [
    'name' => 'product-second-block',
    'title' => Yii::t('text', 'Product Second Block'),
    'preview_image' => Yii::getAlias('@web').'/application/templates/frontend/base/modules/text/layouts/product-second-block/preview.png',
    'viewFile' => '@app/templates/frontend/base/modules/text/layouts/product-second-block/view.php',
    'settings' => [
        'poster' => [
            'type' => 'mediaInput',
            'value' => 'video/all.jpg'
        ],
        'video' => [
            'type' => 'mediaInput',
            'value' => 'video/blocks/720p/all.mp4'
        ],
    ],
];