<?php
/**
 * File name: view.php
 *
 * @var $model \app\modules\text\models\Text;
 */

use yii\helpers\Url;

$isHome = (Yii::$app->request->baseUrl.'/index' == Url::to([''])) ? true : false;
?>

<div class="work-works-block clearfix">
    <aside class="aside-left"></aside>
    <div class="works-block wwb-content clearfix">
        <div class="top-home-slider portfolio-slider slick-initialized slick-slider">
            <div aria-live="polite" class="slick-list draggable"><div class="slick-track" role="listbox" style="opacity: 1; width: 1184px; transform: translate3d(0px, 0px, 0px);"><div class="th-slider-item-wrapper slick-slide slick-current slick-active" data-slick-index="0" aria-hidden="false" tabindex="-1" role="option" aria-describedby="slick-slide00" style="width: 1184px;">
                        <div class="th-slider-item">
                            <video class="video portrait" poster="<?= $model->settings['poster']['value'] ?>" preload="auto">
                                <source type="video/mp4" src="<?= $model->settings['video']['value'] ?>">
                                <p>Your user agent does not support the HTML5 Video element.</p>
                            </video>
                            <span class="video-play-button"></span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>