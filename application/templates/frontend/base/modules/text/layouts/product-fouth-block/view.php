<?php
/**
 * File name: view.php
 *
 * @var $model \app\modules\text\models\Text;
 */

use yii\helpers\Url;

$isHome = (Yii::$app->request->baseUrl.'/index' == Url::to([''])) ? true : false;
?>

<div class="work-benefits-block">
    <ul class="wbb-cnt">
        <?php if($model->getSetting('item1', 0)):?>
            <li id="<?= $model->settings['item1Id']['value'] ?>" class="wbb-item clearfix" data-video="<?= $model->settings['item1Video']['value'] ?>" data-poster="<?= $model->settings['item1Poster']['value'] ?>">
                <div class="kwadrat">
                    <div class="trojkat">
                        <div class="trojkat_bg">
                            <div class="trojkat_bg_2"></div>
                        </div>
                    </div>
                </div>
                <div class="wbb-h3-wrapper clearfix">
                    <h3 class="wbb-h3"><?= $model->settings['item1Title']['value'] ?></h3>
                </div>
                <span class="wbb-item-text"><?= $model->settings['item1Text']['value'] ?></span>
            </li>
        <?php endif; ?>

        <?php if($model->getSetting('item2', 0)):?>
            <li id="<?= $model->settings['item2Id']['value'] ?>" class="wbb-item clearfix" data-video="<?= $model->settings['item2Video']['value'] ?>" data-poster="<?= $model->settings['item2Poster']['value'] ?>">
                <div class="kwadrat">
                    <div class="trojkat">
                        <div class="trojkat_bg">
                            <div class="trojkat_bg_2"></div>
                        </div>
                    </div>
                </div>
                <div class="wbb-h3-wrapper clearfix">
                    <h3 class="wbb-h3"><?= $model->settings['item2Title']['value'] ?></h3>
                </div>
                <span class="wbb-item-text"><?= $model->settings['item2Text']['value'] ?></span>
            </li>
        <?php endif; ?>

        <?php if($model->getSetting('item3', 0)):?>
            <li id="<?= $model->settings['item3Id']['value'] ?>" class="wbb-item clearfix" data-video="<?= $model->settings['item3Video']['value'] ?>" data-poster="<?= $model->settings['item3Poster']['value'] ?>">
                <div class="kwadrat">
                    <div class="trojkat">
                        <div class="trojkat_bg">
                            <div class="trojkat_bg_2"></div>
                        </div>
                    </div>
                </div>
                <div class="wbb-h3-wrapper clearfix">
                    <h3 class="wbb-h3"><?= $model->settings['item3Title']['value'] ?></h3>
                </div>
                <span class="wbb-item-text"><?= $model->settings['item3Text']['value'] ?></span>
            </li>
        <?php endif; ?>

        <?php if($model->getSetting('item4', 0)):?>
            <li id="<?= $model->settings['item4Id']['value'] ?>" class="wbb-item clearfix" data-video="<?= $model->settings['item4Video']['value'] ?>" data-poster="<?= $model->settings['item4Poster']['value'] ?>">
                <div class="kwadrat">
                    <div class="trojkat">
                        <div class="trojkat_bg">
                            <div class="trojkat_bg_2"></div>
                        </div>
                    </div>
                </div>
                <div class="wbb-h3-wrapper clearfix">
                    <h3 class="wbb-h3"><?= $model->settings['item4Title']['value'] ?></h3>
                </div>
                <span class="wbb-item-text"><?= $model->settings['item4Text']['value'] ?></span>
            </li>
        <?php endif; ?>

        <?php if($model->getSetting('item5', 0)):?>
            <li id="<?= $model->settings['item5Id']['value'] ?>" class="wbb-item clearfix" data-video="<?= $model->settings['item5Video']['value'] ?>" data-poster="<?= $model->settings['item5Poster']['value'] ?>">
                <div class="kwadrat">
                    <div class="trojkat">
                        <div class="trojkat_bg">
                            <div class="trojkat_bg_2"></div>
                        </div>
                    </div>
                </div>
                <div class="wbb-h3-wrapper clearfix">
                    <h3 class="wbb-h3"><?= $model->settings['item5Title']['value'] ?></h3>
                </div>
                <span class="wbb-item-text"><?= $model->settings['item5Text']['value'] ?></span>
            </li>
        <?php endif; ?>

        <?php if($model->getSetting('item6', 0)):?>
            <li id="<?= $model->settings['item6Id']['value'] ?>" class="wbb-item clearfix" data-video="<?= $model->settings['item6Video']['value'] ?>" data-poster="<?= $model->settings['item6Poster']['value'] ?>">
                <div class="kwadrat">
                    <div class="trojkat">
                        <div class="trojkat_bg">
                            <div class="trojkat_bg_2"></div>
                        </div>
                    </div>
                </div>
                <div class="wbb-h3-wrapper clearfix">
                    <h3 class="wbb-h3"><?= $model->settings['item6Title']['value'] ?></h3>
                </div>
                <span class="wbb-item-text"><?= $model->settings['item6Text']['value'] ?></span>
            </li>
        <?php endif; ?>

        <?php if($model->getSetting('item7', 0)):?>
            <li id="<?= $model->settings['item7Id']['value'] ?>" class="wbb-item clearfix" data-video="<?= $model->settings['item7Video']['value'] ?>" data-poster="<?= $model->settings['item7Poster']['value'] ?>">
                <div class="kwadrat">
                    <div class="trojkat">
                        <div class="trojkat_bg">
                            <div class="trojkat_bg_2"></div>
                        </div>
                    </div>
                </div>
                <div class="wbb-h3-wrapper clearfix">
                    <h3 class="wbb-h3"><?= $model->settings['item7Title']['value'] ?></h3>
                </div>
                <span class="wbb-item-text"><?= $model->settings['item7Text']['value'] ?></span>
            </li>
        <?php endif; ?>
    </ul>
</div>