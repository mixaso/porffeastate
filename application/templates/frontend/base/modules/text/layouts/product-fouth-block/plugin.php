<?php
/**
 * File name: plugin.php
 */

return [
    'name' => 'product-fouth-block',
    'title' => Yii::t('text', 'Product Fouth Block'),
    'preview_image' => Yii::getAlias('@web').'/application/templates/frontend/base/modules/text/layouts/product-fouth-block/preview.png',
    'viewFile' => '@app/templates/frontend/base/modules/text/layouts/product-fouth-block/view.php',
    'settings' => [
        'item1' => [
            'type' => 'checkbox',
            'value' => ''
        ],
        'item1Id' => [
            'type' => 'textInput',
            'value' => 'interactive'
        ],
        'item1Poster' => [
            'type' => 'mediaInput',
            'value' => 'video/blocks/1INTERACTIVE-MODEL-OF-PROPERTY.jpg'
        ],
        'item1Video' => [
            'type' => 'mediaInput',
            'value' => 'video/blocks/720p/1INTERACTIVE-MODEL-OF-PROPERTY.mp4'
        ],
        'item1Title' => [
            'type' => 'textInput',
            'value' => 'Interactive model of property'
        ],
        'item1Text' => [
            'type' => 'textarea',
            'value' => 'Explore the whole development area, the building and surrounding, view from any angle, rotate and zoom as you wish, examine every corner of the project, take your time to get the feel of space.'
        ],
        'item2' => [
            'type' => 'checkbox',
            'value' => ''
        ],
        'item2Id' => [
            'type' => 'textInput',
            'value' => 'interactiveFloor'
        ],
        'item2Poster' => [
            'type' => 'mediaInput',
            'value' => 'video/blocks/3INTERACTIVE-FLOOR-(LEVEL)-PLAN-WITH-APARTMENTS.jpg'
        ],
        'item2Video' => [
            'type' => 'mediaInput',
            'value' => 'video/blocks/720p/3INTERACTIVE-FLOOR-(LEVEL)-PLAN-WITH-APARTMENTS.mp4'
        ],
        'item2Title' => [
            'type' => 'textInput',
            'value' => 'Interactive Floor (level) plan with apartments'
        ],
        'item2Text' => [
            'type' => 'textarea',
            'value' => 'Look closer at a detailed level plan, rotate and zoom to understand the placement of any apartment on the floor.'
        ],
        'item3' => [
            'type' => 'checkbox',
            'value' => ''
        ],
        'item3Id' => [
            'type' => 'textInput',
            'value' => 'walkthrough'
        ],
        'item3Poster' => [
            'type' => 'mediaInput',
            'value' => 'video/blocks/2WALKTHROUGH-MODE-IN-APARTMENT.jpg'
        ],
        'item3Video' => [
            'type' => 'mediaInput',
            'value' => 'video/blocks/720p/2WALKTHROUGH-MODE-IN-APARTMENT.mp4'
        ],
        'item3Title' => [
            'type' => 'textInput',
            'value' => 'Walkthrough mode in apartment'
        ],
        'item3Text' => [
            'type' => 'textarea',
            'value' => 'Get inside any apartment, walk freely inside of everyroom. Enjoy the views from the windows from this particular point of the building, during any particular time of the day.'
        ],
        'item4' => [
            'type' => 'checkbox',
            'value' => ''
        ],
        'item4Id' => [
            'type' => 'textInput',
            'value' => 'hotspots'
        ],
        'item4Poster' => [
            'type' => 'mediaInput',
            'value' => 'video/blocks/5HOTSPOTS_1.jpg'
        ],
        'item4Video' => [
            'type' => 'mediaInput',
            'value' => 'video/blocks/720p/5HOTSPOTS_1.mp4'
        ],
        'item4Title' => [
            'type' => 'textInput',
            'value' => 'Hotspots'
        ],
        'item4Text' => [
            'type' => 'textarea',
            'value' => 'Get to the key locations, objects and amenities of the projects in a single click. Examine the location carefully.'
        ],
        'item5' => [
            'type' => 'checkbox',
            'value' => ''
        ],
        'item5Id' => [
            'type' => 'textInput',
            'value' => 'flythrough'
        ],
        'item5Poster' => [
            'type' => 'mediaInput',
            'value' => 'video/blocks/4FLYTHROUGH_2.jpg'
        ],
        'item5Video' => [
            'type' => 'mediaInput',
            'value' => 'video/blocks/720p/4FLYTHROUGH_2.mp4'
        ],
        'item5Title' => [
            'type' => 'textInput',
            'value' => 'Flythrough'
        ],
        'item5Text' => [
            'type' => 'textarea',
            'value' => 'Flythrough mode for a hands-free presentation of the project. Relax and let us guide you through the most beautiful spots on development area.'
        ],
        'item6' => [
            'type' => 'checkbox',
            'value' => ''
        ],
        'item6Id' => [
            'type' => 'textInput',
            'value' => 'videoGallery'
        ],
        'item6Poster' => [
            'type' => 'mediaInput',
            'value' => 'video/blocks/6VIDEO-AND-GALLERY-INTEGRATION.jpg'
        ],
        'item6Video' => [
            'type' => 'mediaInput',
            'value' => 'video/blocks/720p/6VIDEO-AND-GALLERY-INTEGRATION.mp4'
        ],
        'item6Title' => [
            'type' => 'textInput',
            'value' => 'VIDEO and GALLERY Integration'
        ],
        'item6Text' => [
            'type' => 'textarea',
            'value' => 'Make your media materials like HD photos or videos work in the panel in separate built-in modules.'
        ],
        'item7' => [
            'type' => 'checkbox',
            'value' => ''
        ],
        'item7Id' => [
            'type' => 'textInput',
            'value' => 'map'
        ],
        'item7Poster' => [
            'type' => 'mediaInput',
            'value' => 'video/blocks/7INTERACTIVE-MAP.jpg'
        ],
        'item7Video' => [
            'type' => 'mediaInput',
            'value' => 'video/blocks/720p/7INTERACTIVE-MAP.mp4'
        ],
        'item7Title' => [
            'type' => 'textInput',
            'value' => 'Interactive map'
        ],
        'item7Text' => [
            'type' => 'textarea',
            'value' => 'Learn about the location of development on the city map, discover the surrounding infrastructure and get to know more about any object in the area.'
        ],
    ],
];