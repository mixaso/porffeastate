<?php
/**
 * File name: view.php
 *
 * @var $model \app\modules\text\models\Text;
 */

use yii\helpers\Url;

$isHome = (Yii::$app->request->baseUrl.'/index' == Url::to([''])) ? true : false;
?>

<div class="work-reasons-block clearfix">
    <h2><?= $model->settings['heading']['value'] ?></h2>
    <div id="wrb-slider">
        <div class="wrb-slider-block" style="background-image:url('<?= $model->settings['firstBlockBg']['value'] ?>')">
            <div class="wrb-slider-layer"></div>
            <h3 class="wrb-slider-title"><?= $model->settings['firstBlockTitle']['value'] ?></h3>
            <div class="wrb-slider-text"><?= $model->settings['firstBlockText']['value'] ?></div>
        </div>
        <div class="wrb-slider-block" style="background-image:url('<?= $model->settings['secondBlockBg']['value'] ?>')">
            <div class="wrb-slider-layer"></div>
            <h3 class="wrb-slider-title"><?= $model->settings['secondBlockTitle']['value'] ?></h3>
            <div class="wrb-slider-text"><?= $model->settings['secondBlockText']['value'] ?></div>
        </div>
        <div class="wrb-slider-block" style="background-image:url('<?= $model->settings['thirdBlockBg']['value'] ?>')">
            <div class="wrb-slider-layer"></div>
            <h3 class="wrb-slider-title"><?= $model->settings['thirdBlockTitle']['value'] ?></h3>
            <div class="wrb-slider-text"><?= $model->settings['thirdBlockText']['value'] ?></div>
        </div>
    </div>
</div>