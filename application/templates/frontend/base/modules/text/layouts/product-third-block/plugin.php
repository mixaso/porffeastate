<?php
/**
 * File name: plugin.php
 */

return [
    'name' => 'product-third-block',
    'title' => Yii::t('text', 'Product Third Block'),
    'preview_image' => Yii::getAlias('@web').'/application/templates/frontend/base/modules/text/layouts/product-third-block/preview.png',
    'viewFile' => '@app/templates/frontend/base/modules/text/layouts/product-third-block/view.php',
    'settings' => [
        'heading' => [
            'type' => 'textInput',
            'value' => 'WHY PROF.ESTATE'
        ],
        'firstBlockBg' => [
            'type' => 'mediaInput',
            'value' => 'images/work-dark/bitmap.jpg'
        ],
        'firstBlockTitle' => [
            'type' => 'textInput',
            'value' => 'Engage'
        ],
        'firstBlockText' => [
            'type' => 'textarea',
            'value' => 'Increasing number (75%) of 
                home buyers tend to look for interactive visualisations of their future home during search and decision making process.'
        ],
        'secondBlockBg' => [
            'type' => 'mediaInput',
            'value' => 'images/work-dark/v-s-g-jogging-lance-v-6-preview.jpg'
        ],
        'secondBlockTitle' => [
            'type' => 'textInput',
            'value' => 'Impress'
        ],
        'secondBlockText' => [
            'type' => 'textarea',
            'value' => 'Interactive Touch Panels bring visualization to new level, where the whole future property project is at customers\' fingertips.'
        ],
        'thirdBlockBg' => [
            'type' => 'mediaInput',
            'value' => 'images/work-dark/rooftop-small-tower-to-scene-seeing.jpg'
        ],
        'thirdBlockTitle' => [
            'type' => 'textInput',
            'value' => 'Sale'
        ],
        'thirdBlockText' => [
            'type' => 'textarea',
            'value' => 'The tool we provide allows to captivate a prospective home buyer just at sales office, giving opportunity to experience the property right at the spot and speed up the property purchase.'
        ],
    ],
];