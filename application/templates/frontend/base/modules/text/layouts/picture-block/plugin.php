<?php
/**
 * File name: plugin.php
 */

return [
    'name' => 'picture-block',
    'title' => Yii::t('text', 'Picture Block'),
    'preview_image' => Yii::getAlias('@web').'/application/templates/frontend/base/modules/text/layouts/picture-block/preview.png',
    'viewFile' => '@app/templates/frontend/base/modules/text/layouts/picture-block/view.php',
    'settings' => [
        'heading' => [
            'type' => 'textInput',
            'value' => 'Where can you use this solution?'
        ],
        'image1' => [
            'type' => 'mediaInput',
            'value' => 'images/stock-photo-businesswoman-with-cell-telephone-in-hand-touching-big-computer-monitor-while-standing-in-modern-379971679@2x.jpg'
        ],
        'image1Alt' => [
            'type' => 'textInput',
            'value' => ''
        ],
        'description1' => [
            'type' => 'textarea',
            'value' => 'Any big screen television in your sales office can be turned into a touch screen giving your sales team an opportunity to have an impressive demonstration and have customers interact with the project. Higher engagement leads to better sales results!'
        ],
        'image2' => [
            'type' => 'mediaInput',
            'value' => 'images/stock-photo-businesswoman-with-cell-telephone-in-hand-touching-big-computer-monitor-while-standing-in-modern-379971679-copy@2x.jpg'
        ],
        'image2Alt' => [
            'type' => 'textInput',
            'value' => ''
        ],
        'description2' => [
            'type' => 'textarea',
            'value' => 'With our solution there is no need for the field agents and brokers to bring the customer to the sales office anymore. Using their tablets they can make a full demonstration of the project anywhere in the world! Even if the buyer is in a foreign country!'
        ],
    ],
];
