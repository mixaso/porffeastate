<?php
/**
 * File name: view.php
 *
 * @var $model \app\modules\text\models\Text;
 */

use yii\helpers\Url;

$isHome = (Yii::$app->request->baseUrl.'/index' == Url::to([''])) ? true : false;
?>

<div class="picture-block clearfix">
    <div class="aside-left">
        <h2 class="pb-h2"><?= $model->settings['heading']['value'] ?></h2>
    </div>
    <div class="content pb-content">
        <div class="pb-image-wrapper clearfix">
            <div class="pb-image clearfix">
                <img  class=" col-sm-push-12" src="<?= $model->settings['image1']['value'] ?>" alt="<?= $model->settings['image1Alt']['value']
                ?>">
                <div class="pb-image-text col-sm-pull-12">
                    <?= $model->settings['description1']['value'] ?>
                </div>
            </div>
            <div class="pb-image clearfix">
                <img src="<?= $model->settings['image2']['value'] ?>" alt="<?= $model->settings['image2Alt']['value']
                ?>">
                <div class="pb-image-text">
                    <?= $model->settings['description2']['value'] ?>
                </div>
            </div>
        </div>
    </div>
</div>