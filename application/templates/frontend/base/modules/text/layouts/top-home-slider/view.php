<?php
/**
 * File name: view.php
 *
 * @var $model \app\modules\text\models\Text;
 */

use yii\helpers\Url;

$isHome = (Yii::$app->request->baseUrl.'/index' == Url::to([''])) ? true : false;
?>

<div class="top-home-slider">
    <div class="th-slider-item">
        <video class="video" poster="<?= $model->settings['poster']['value'] ?>" preload="auto" muted="muted" loop="loop">
            <source type="video/mp4" src="<?= $model->settings['video']['value'] ?>"/>
            <p>Your user agent does not support the HTML5 Video element.</p>
        </video>
        <div class="blue-gradient-wrapper">
            <div class="blue-gradient"></div>
            <div class="th-slider-item-text-block">
                <h1 class="th-slider-title"><?= $model->settings['bigText']['value'] ?></h1>
                <span class="th-slider-text"><?= $model->settings['smallText']['value'] ?></span>
            </div>
            <a href="#<?= $model->settings['idBlock']['value'] ?>" class="btn formInhomeBTN"><?= $model->settings['buttonText']['value'] ?></a>
        </div>
    </div>
</div>