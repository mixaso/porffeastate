<?php
/**
 * File name: plugin.php
 */

return [
    'name' => 'top-home-slider',
    'title' => Yii::t('text', 'Top Home Slider'),
    'preview_image' => Yii::getAlias('@web').'/application/templates/frontend/base/modules/text/layouts/top-home-slider/preview.png',
    'viewFile' => '@app/templates/frontend/base/modules/text/layouts/top-home-slider/view.php',
    'settings' => [
        'poster' => [
            'type' => 'mediaInput',
            'value' => 'video/v-6-copy.jpg'
        ],
        'video' => [
            'type' => 'mediaInput',
            'value' => 'video/blocks/720p/IMG_2.mp4'
        ],
        'bigText' => [
            'type' => 'textarea',
            'value' => 'The most efficient tool for selling off-plan properties!'
        ],
        'smallText' => [
            'type' => 'textarea',
            'value' => 'Our interactive touch screen solution for property developers combines all sales materials:<br> renders, videos, floor plans into one efficient sales solution, which can be used either on touchscreens in sales offices or given to brokers to use on tablets while meeting customers.'
        ],
        'idBlock' => [
            'type' => 'textInput',
            'value' => 'formInHome'
        ],
        'buttonText' => [
            'type' => 'textInput',
            'value' => 'BOOK A LIVE DEMO'
        ],
    ],
];
