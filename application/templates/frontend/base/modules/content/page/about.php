<?php
/**
 *  File name: view.php
 *
 * @var $model \app\modules\content\models\ContentPages;
 */


$this->bodyClass = ['page-'.$model->id];

$this->setSeoData($model->title, $model->description, '');
?>

<section class="<?= $model->slug ?>">
        <?= $model->content ?>
</section>
