<?php
/**
 * File name: view.php
 *
 * @var $model \app\modules\content\models\ContentCategory;
 * @var $dataProvider \yii\data\ActiveDataProvider;
 * @var $breadcrumbs array
 */

use yii\widgets\ListView;
use yii\helpers\Html;
use yii\helpers\Url;
use \app\modules\content\models\ContentTagAssn;

$this->setSeoData(($model->meta_title != '')?$model->meta_title:$model->title, $model->meta_description, $model->meta_keywords);

/** @var \app\modules\menu\models\MenuItem $menu */
$menu = Yii::$app->menuManager->getActiveMenu();
if ($menu) {
   // $this->params['breadcrumbs'] = $menu->getBreadcrumbs(false);
}
$this->title = $model->title;
$cat = \app\modules\content\models\ContentCategory::find()->published()
       ->all();
$tag = \app\modules\content\models\ContentTags::find()->all();
?>

<div class="row blog">
    <div class="blog__header clearfix">
        <div class="aside-left blog__col">
            <h1>Our blog</h1>
        </div>
        <div class="content blog__col clearfix">
            <div class="blog_desc">
                <span>

                </span>
            </div>
        </div>
    </div>
    <div class="aside-left blog__col">
        <ul class="blog__list">
            <?php
                foreach ($cat as $categ) {
                    echo Html::tag('li',Html::a($categ->title, $categ->getFrontendViewLink($categ)),['class'=>'blog__list_item']);
                }
            ?>
        </ul>
        <?php foreach ($tag as $tagName):
            $count = ContentTagAssn::find()->where(['content_tags_id'=>$tagName->id])->count();
            ?>
            <?php if ($count > 0): ?>
                  <div class="blog__list_cntnrTag">
                      <a href="<?= Url::to(['/content/article/tag', 'tag' => mb_strtolower($tagName->name)]) ?>" class="blog__list_tag">
                          <?= $tagName->name.' - <span>'.$count.'</span>' ?>
                      </a>
                  </div>
            <?php endif; ?>
        <?php endforeach;?>
    </div>

    <div class="content clearfix">
        <section class="blog__items">
            <?= ListView::widget([
                'dataProvider' => $dataProvider,
                'itemOptions' => ['class' => 'col-sm-6 col-md-4 blog__item'],
                'itemView' => '_item',
                'layout' => "{items}</div>\n<div><div class='text-center relative'>{pager}</div>",
                'pager' => [
                    'options' => [
                        'class' => 'pagination'
                    ]
                ]
            ]); ?>
        </section>
    </div>
</div>

