<?php
/**
 * Created by Vladimir Hryvinskyy.
 * Site: http://codice.in.ua/
 * Date: 21.11.2016
 * Project: kardamon_blog
 * File name: _item.php
 *
 * @var $model \app\modules\content\models\ContentArticles
 */

use yii\helpers\Url;
use yii\bootstrap\Html;
?>
<div class="col-sm-6 col-md-4 blog__item" data-key="1">
          <a href="<?= Url::to(['/content/article/view', 'catslug' => $model->category->slug, 'slug' => $model->slug]) ?>" >
              <div class="blog__item_img" style="background: url('<?= $model->getThumbUploadUrl('image') ?>') center; background-size: cover;height:338px;">
              </div>
              <div class="blog__item__cntnt">
                  <div class="blog__item__cntnt_ttl">
                       <span><?= $model->title ?></span>
                  </div>
                  <div class="blog__item__cntnt_content">
                       <?= $model->description ?>
                  </div>
              </div>
          </a>
</div>
