<?php
/**
 * Created by Vladimir Hryvinskyy.
 * Site: http://codice.in.ua/
 * Date: 21.11.2016
 * Project: kardamon_blog
 * File name: tag.php
 */
use app\modules\content\models\ContentTagAssn;
use app\modules\content\models\ContentCategory;
use yii\helpers\Html;
use app\modules\content\models\ContentTags;
use yii\helpers\Url;
?>

<?php
$cat = ContentCategory::find()->published()->all();
$tag = ContentTags::find()->all();
?>
<div class="row blog">
    <div class="blog__header clearfix">
        <div class="aside-left blog__col">
            <h1>Our blog</h1>
        </div>
        <div class="content blog__col clearfix">
            <div class="blog_desc">
                <span>

                </span>
            </div>
        </div>
    </div>
    <div class="aside-left blog__col">
        <ul class="blog__list">
            <?php
            foreach ($cat as $categ) {
                echo Html::tag('li',Html::a($categ->title, $categ->getFrontendViewLink($categ)),['class'=>'blog__list_item']);
            }
            ?>
        </ul>
        <?php foreach ($tag as $tagName):
            $count = ContentTagAssn::find()->where(['content_tags_id'=>$tagName->id])->count();
            ?>
            <?php if ($count > 0): ?>
                 <div class="blog__list_cntnrTag">
                     <a href="<?= Url::to(['/content/article/tag', 'tag' => mb_strtolower($tagName->name)]) ?>" class="blog__list_tag">
                         <?= $tagName->name.' - <span>'.$count.'</span>' ?>
                     </a>
                 </div>
            <?php endif; ?>
        <?php endforeach;?>
    </div>

    <div class="content clearfix">
       <section class="blog__items">
         <div class="list-view">
                     <?php foreach ($articles as $article):?>
                         <?= $this->render('_item.php', ['model' => $article]); ?>
                     <?php endforeach;?>
         </div>
           <div><div class="text-center relative"></div></div>
       </section>
   </div>
</div>