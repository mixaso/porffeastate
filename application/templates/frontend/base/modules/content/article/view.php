<?php

/**
 * File name: view.php
 *
 * @var $model \app\modules\content\models\ContentArticles;
 * @var $categoryModel \app\modules\content\models\ContentCategory;
 * @var $breadcrumbs \yii\widgets\Breadcrumbs;
 * @var $this \yii\web\View;
 */

use yii\helpers\Url;
use yii\helpers\StringHelper;
use yii\bootstrap\Html;
use app\modules\content\models\ContentArticles;
use app\modules\content\models\ContentTags;
use app\modules\content\models\ContentTagAssn;


$rating_id = 'rating_'.$model->id;

$meta_title = ($model->meta_title != '') ? $model->meta_title : $model->title;
$meta_description = StringHelper::truncate((($model->meta_description != '') ? $model->meta_description : strip_tags($model->description)), '140', '');
$meta_keywords = ($model->meta_keywords != '') ? $model->meta_keywords : implode(', ', explode(' ', $model->title));

$this->setSeoData($meta_title, $meta_description, $meta_keywords);

$articles = ContentArticles::find()->orderBy(['published_at'=>SORT_DESC])
            ->published()
            ->limit(6)
            ->all();
$tag = ContentTags::find()->all();

$this->params['breadcrumbs'][] = [
    'url'   => ['/blog'],
    'label' => 'Blog',
];
$this->params['breadcrumbs'][] = $model->title;
?>


<div class="container article">
    <?php if(isset($this->params['breadcrumbs'])): ?>
        <div class="breadcrumbs caseItem_breadcrumbs pull-left">
            <?= \yii\widgets\Breadcrumbs::widget([
                'options' => ['class' => 'inline-layout'],
                'itemTemplate' => "<li>{link}/</li>\n",
                'activeItemTemplate' => "<li><span>{link}</span></li>\n",
                'links' => $this->params['breadcrumbs'],
                'homeLink' => false,
            ]);?>
        </div>
        <?php $this->params['breadcrumbs'] = null; ?>
    <?php endif; ?>
    <div class="article_back pull-right"><?php echo Html::a('Back',Yii::$app->request->referrer,['class'=>'']);?></div>
    <h2>Our blog</h2>
    <div class="col-sm-3">
        <div class="article__list_ttl">Last Articles</div>
        <ul class="article__list">
            <?php
            foreach ($articles as $art) {
            echo Html::tag('li',Html::a($art->title, $art->getFrontendViewLink($art)),['class'=>'article__list_item']);
            }
            ?>
        </ul>
    </div>
    <div class="col-sm-9">
        <img src="<?= $model->getUploadUrl('image') ?>" alt="<?= $model->title ?>" class="article_img">
        <div class="article__date">
            <span class="pull-left">
                <?= date("d.m.Y", $model->published_at) ?>
            </span>
            <script type="text/javascript">(function() {
                    if (window.pluso)if (typeof window.pluso.start == "function") return;
                    if (window.ifpluso==undefined) { window.ifpluso = 1;
                        var d = document, s = d.createElement('script'), g = 'getElementsByTagName';
                        s.type = 'text/javascript'; s.charset='UTF-8'; s.async = true;
                        s.src = ('https:' == window.location.protocol ? 'https' : 'http')  + '://share.pluso.ru/pluso-like.js';
                        var h=d[g]('body')[0];
                        h.appendChild(s);
                    }})();</script>
            <div class="pluso" style="float:right;" data-background="#ebebeb" data-options="medium,square,line,horizontal,nocounter,theme=02" data-services="facebook,linkedin,twitter,google"></div>
        </div>
        <h1 class="article__ttl">
            <?= $model->title ?>
        </h1>
        <div class="article__txt">
            <?= $model->content ?>
        </div>
        <div class="article__tags">
            <div class="cntnt__tags__text">
                <?php foreach (explode(',', $model->tagNames) as $tagName):?>
                    <span href="<?= Url::to(['/content/article/tag', 'tag' => trim($tagName)]) ?>">
                        <?= trim($tagName) ?>
                    </span>
                <?php endforeach;?>
            </div>
        </div>
    </div>
</div>
<style>
    .pluso-more {
        display:none!important;
    }
</style>
