<?php
/**
 * File name: tag.php
 */
use app\modules\casecontent\models\ContentTagAssn;
use app\modules\casecontent\models\ContentCategory;
use yii\helpers\Html;
use app\modules\casecontent\models\ContentTags;
use yii\helpers\Url;
?>

<?php
$cat = ContentCategory::find()->published()->all();
$tag = ContentTags::find()->all();
?>
<div class="row case">
    <div class="case__header clearfix">
        <div class="aside-left case__col">
            <h1>Our pride</h1>
        </div>
        <div class="content case__col clearfix">
            <div class="case_desc">
                <span>
                    Every business case for us is a great opportunity not only to meet the KPIs, but also to use our global know-how in practice.
                </span>
            </div>
        </div>
    </div>
    <div class="aside-left case__col">
        <ul class="case__list">
            <?php
            foreach ($cat as $categ) {
                echo Html::tag('li',Html::a($categ->title, $categ->getFrontendViewLink($categ)),['class'=>'case__list_item']);
            }
            ?>
        </ul>
        <?php foreach ($tag as $tagName):
            $count = ContentTagAssn::find()->where(['content_tags_id'=>$tagName->id])->count();
            ?>
            <?php if ($count > 0): ?>
                <div class="case__list_cntnrTag">
                    <a href="<?= Url::to(['/casecontent/article/tag', 'tag' => mb_strtolower($tagName->name)]) ?>" class="blog__list_tag">
                        <?= $tagName->name.' - <span>'.$count.'</span>' ?>
                    </a>
                </div>
            <?php endif; ?>
        <?php endforeach;?>
    </div>

      <div class="content clearfix">
         <section class="blog__items">
            <div class="list-view">
                    <?php foreach ($articles as $article):?>
                        <?= $this->render('_item.php', ['model' => $article]); ?>
                    <?php endforeach;?>
            </div>
            <div><div class="text-center relative"></div></div>
         </section>
      </div>

</div>