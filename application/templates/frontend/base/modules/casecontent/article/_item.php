<?php
/**
 * Created by Vladimir Hryvinskyy.
 * Site: http://codice.in.ua/
 * Date: 21.11.2016
 * Project: kardamon_blog
 * File name: _item.php
 *
 * @var $model \app\modules\content\models\ContentArticles
 */

use yii\helpers\Url;
use yii\bootstrap\Html;
use \app\modules\casecontent\models\ContentTagAssn;

?>
<div class="col-sm-6 col-md-4 case__item" data-key="1">
  <div class="<?=$model->getField('comingsoon');?>">
    <a href="<?= Url::to(['/casecontent/article/view', 'catslug' => $model->category->slug, 'slug' => $model->slug]) ?>" style="background-image: url('<?= $model->getThumbUploadUrl('image') ?>'); background-size: cover;
            background-position: center;">
        <div class="case__item_img" style="height: 337px;">
            <div class="case__item__cntnt">
                <div class="case__item__cntnt_ttl">
                <span>
                    <?= $model->title ?>
                </span>
                </div>
                <div class="case__item__cntnt_content">
                    <?= $model->description ?>
                </div>
            </div>
        </div>
    </a>
  </div>
</div>
