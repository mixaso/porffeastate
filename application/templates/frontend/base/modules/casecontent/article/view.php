<?php

/**
 * File name: view.php
 *
 * @var $model \app\modules\content\models\ContentArticles;
 * @var $categoryModel \app\modules\content\models\ContentCategory;
 * @var $breadcrumbs \yii\widgets\Breadcrumbs;
 * @var $this \yii\web\View;
 */

use yii\helpers\Url;
use yii\helpers\StringHelper;
use yii\bootstrap\Html;
use app\modules\casecontent\models\ContentArticles;
use app\modules\casecontent\models\ContentTags;
use app\modules\casecontent\models\ContentTagAssn;


$rating_id = 'rating_'.$model->id;

$meta_title = ($model->meta_title != '') ? $model->meta_title : $model->title;
$meta_description = StringHelper::truncate((($model->meta_description != '') ? $model->meta_description : strip_tags($model->description)), '140', '');
$meta_keywords = ($model->meta_keywords != '') ? $model->meta_keywords : implode(', ', explode(' ', $model->title));

$this->setSeoData($meta_title, $meta_description, $meta_keywords);

$articles = ContentArticles::find()->orderBy(['published_at'=>SORT_DESC])
            ->published()
            ->limit(6)
            ->all();
$tag = ContentTags::find()->all();
?>

<?= $model->content ?>
