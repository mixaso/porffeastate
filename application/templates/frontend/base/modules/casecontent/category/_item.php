<?php
/**
 * File name: _item.php
 *
 * @var $model \app\modules\content\models\ContentArticles
 */

use yii\helpers\Url;
use yii\bootstrap\Html;

?>
<div class="<?=$model->getField('comingsoon');?>">
    <?php
        $aurl = Url::to(['/casecontent/article/view', 'catslug' => $model->category->slug, 'slug' => $model->slug]);
        if ($model->getField('comingsoon')) {
            $aurl = '';
        }
    ?>
    <a href="<?= $aurl ?>" style="background-image: url('<?= $model->getThumbUploadUrl('image') ?>'); background-size: cover;
            background-position: center;">
        <div class="case__item_img">
            <div class="case__item__cntnt">
                <div class="case__item__cntnt_ttl">
                <span>
                    <?= $model->title ?>
                </span>
                </div>
                <div class="case__item__cntnt_content">
                    <?= $model->description ?>
                </div>
            </div>
        </div>
    </a>
</div>
