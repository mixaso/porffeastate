<?php
/**
 * File name: view.php
 *
 * @var $model \app\modules\content\models\ContentCategory;
 * @var $dataProvider \yii\data\ActiveDataProvider;
 * @var $breadcrumbs array
 */

use yii\widgets\ListView;
use yii\helpers\Html;
use yii\helpers\Url;
use \app\modules\casecontent\models\ContentTagAssn;

$this->setSeoData(($model->meta_title != '')?$model->meta_title:$model->title, $model->meta_description, $model->meta_keywords);

/** @var \app\modules\menu\models\MenuItem $menu */
$menu = Yii::$app->menuManager->getActiveMenu();
if ($menu) {
   // $this->params['breadcrumbs'] = $menu->getBreadcrumbs(false);
}
$this->title = $model->title;
$cat = \app\modules\casecontent\models\ContentCategory::find()->published()
       ->all();
$tag = \app\modules\casecontent\models\ContentTags::find()->all();
?>

<div class="row case">
    <div class="case__header clearfix">
        <div class="aside-left case__col">
            <h1>Our pride</h1>
        </div>
        <div class="content case__col clearfix">
            <div class="case_desc">
                <span>
                    Every business case for us is a great opportunity not only to meet the KPIs, but also to use our global know-how in practice.
                </span>
            </div>
        </div>
    </div>
    <div class="clearfix">
        <div class="aside-left case__col">
            <ul class="case__list">
                <?php
                foreach ($cat as $categ) {
                    echo Html::tag('li',Html::a($categ->title, $categ->getFrontendViewLink($categ)),['class'=>'blog__list_item']);
                }
                ?>
            </ul>
            <?php foreach ($tag as $tagName):
                $count = ContentTagAssn::find()->where(['content_tags_id'=>$tagName->id])->count();
                ?>
                <?php if ($count > 0): ?>
                      <div class="blog__list_cntnrTag">
                          <a href="<?= Url::to(['/casecontent/article/tag', 'tag' => mb_strtolower($tagName->name)]) ?>" class="case__list_tag">
                              <?= $tagName->name.' - <span>'.$count.'</span>' ?>
                          </a>
                      </div>
                <?php endif; ?>
            <?php endforeach;?>
        </div>

        <div class="content clearfix">
            <section class="case__items">
                <?= ListView::widget([
                    'dataProvider' => $dataProvider,
                    'itemOptions' => ['class' => 'col-sm-6 col-md-4 case__item'],
                    'itemView' => '_item',
                    'layout' => "{items}</div>\n<div><div class='text-center relative'>{pager}</div>",
                    'pager' => [
                        'options' => [
                            'class' => 'pagination'
                        ]
                    ]
                ]); ?>
            </section>
        </div>
    </div>
</div>

